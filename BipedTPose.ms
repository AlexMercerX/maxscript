
rollout BipedTPoseRollout "TPose输出FBX"
(
    editText textFBXSuffix "Fbx文件后缀" width:160 text:"_TPose"
    checkbox chkBipedOnly "只输出CS骨骼" default:false
    button btnTPoseExport "输出Fbx到Max文件目录"
    
    fn TraversalChildNode parentNode selectionArray = 
    (
        if parentNode == undefined then
        (
            return false
        )

        for childnode in parentNode.children do
        (
            if childnode.children.count > 0 then
            (
                if not (TraversalChildNode childnode selectionArray) then
                    return false
            )
            else
            (
                append selectionArray childnode
            )
        )
        append selectionArray parentNode
        return true
    )

    fn SelectSkeletonAll = 
    (
        local selectionArray = #()
        local ret = TraversalChildNode $Root selectionArray
        if ret then
        (
            selectMore selectionArray
        )
        return ret
    )

    fn GetAllBipedObjects = 
    (
        local bipedObjects = #()
        for geo in geometry where (isKindOf geo Biped_Object) do
        (
	        append bipedObjects geo
        )
        return bipedObjects
    )

    on btnTPoseExport pressed do
    (
        setquietmode true
        max hold

        local filename = maxfilepath + maxfilename
        if filename == "" then
        (
            setquietmode false
            messagebox("操作失败 Max文件异常!          ")
            return false
        )

        fbxExporterSetParam "Animation" false
        sliderTime = 0

        local rootBone = $Root
        if undefined == rootBone then
        (
            setquietmode false
            messagebox("操作失败 骨架上没有Root点!          ")
            return false
        )

        local BipedBones = GetAllBipedObjects()

        local L_Clavicle = undefined
        local L_UpperArm = undefined
        local L_Forearm = undefined
        local L_Hand = undefined

        local R_Clavicle = undefined
        local R_UpperArm = undefined
        local R_Forearm = undefined
        local R_Hand = undefined

        local L_Finger0Arr = #()
        local L_Finger1Arr = #()
        local L_Finger2Arr = #()
        local L_Finger3Arr = #()
        local L_Finger4Arr = #()

        local R_Finger0Arr = #()
        local R_Finger1Arr = #()
        local R_Finger2Arr = #()
        local R_Finger3Arr = #()
        local R_Finger4Arr = #()

        for BoneNode in BipedBones do
        (
            if (findString BoneNode.name "L Clavicle" != undefined) then
                L_Clavicle = BoneNode
            if (findString BoneNode.name "L UpperArm" != undefined) then
                L_UpperArm = BoneNode
            if (findString BoneNode.name "L Forearm" != undefined) then
                L_Forearm = BoneNode
            if (findString BoneNode.name "L Hand" != undefined) then
                L_Hand = BoneNode
            if (findString BoneNode.name "L Finger0" != undefined) then
                append L_Finger0Arr BoneNode
            if (findString BoneNode.name "L Finger1" != undefined) then
                append L_Finger1Arr BoneNode
            if (findString BoneNode.name "L Finger2" != undefined) then
                append L_Finger2Arr BoneNode
            if (findString BoneNode.name "L Finger3" != undefined) then
                append L_Finger3Arr BoneNode
            if (findString BoneNode.name "L Finger4" != undefined) then
                append L_Finger4Arr BoneNode

            if (findString BoneNode.name "R Clavicle" != undefined) then
                R_Clavicle = BoneNode
            if (findString BoneNode.name "R UpperArm" != undefined) then
                R_UpperArm = BoneNode
            if (findString BoneNode.name "R Forearm" != undefined) then
                R_Forearm = BoneNode
            if (findString BoneNode.name "R Hand" != undefined) then
                R_Hand = BoneNode
            if (findString BoneNode.name "R Finger0" != undefined) then
                append R_Finger0Arr BoneNode
            if (findString BoneNode.name "R Finger1" != undefined) then
                append R_Finger1Arr BoneNode
            if (findString BoneNode.name "R Finger2" != undefined) then
                append R_Finger2Arr BoneNode
            if (findString BoneNode.name "R Finger3" != undefined) then
                append R_Finger3Arr BoneNode
            if (findString BoneNode.name "R Finger4" != undefined) then
                append R_Finger4Arr BoneNode
        )

        if L_Clavicle != undefined then
            biped.settransform L_Clavicle #rotation ((eulerAngles 0 0 0) as quat) false
        if L_UpperArm != undefined then
            biped.settransform L_UpperArm #rotation ((eulerAngles 0 0 0) as quat) false
        if L_Forearm != undefined then
            biped.settransform L_Forearm #rotation ((eulerAngles 0 0 0) as quat) false
        if L_Hand != undefined then
            biped.settransform L_Hand #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger0Arr do
            biped.settransform _bone #rotation ((eulerAngles 0 45 -45) as quat) false
        for _bone in L_Finger1Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger2Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger3Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger4Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        
        if R_Clavicle != undefined then
            biped.settransform R_Clavicle #rotation ((eulerAngles 0 180 0) as quat) false
        if R_UpperArm != undefined then
            biped.settransform R_UpperArm #rotation ((eulerAngles 0 180 0) as quat) false
        if R_Forearm != undefined then
            biped.settransform R_Forearm #rotation ((eulerAngles 0 180 0) as quat) false
        if R_Hand != undefined then
            biped.settransform R_Hand #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger0Arr do
            biped.settransform _bone #rotation ((eulerAngles 0 135 45) as quat) false
        for _bone in R_Finger1Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger2Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger3Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger4Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        
        clearSelection()
        if chkBipedOnly.checked then
        (
            local allBiped = GetAllBipedObjects()
            selectmore allBiped
        )
        else
        (
            SelectSkeletonAll()
        )
        
        filename = substring filename 1 (filename.count-4)
        filename = filename + textFBXSuffix.text + ".fbx"
        local dir = getFilenamePath filename
        if not isDirectoryWriteable dir then makeDir dir all:true

        exportFile filename #noPrompt selectedOnly:true using:FBXEXP

        max fetch
        setquietmode false

        messagebox("成功导出Tpose骨骼！     ")
        return true
    )
)

BipedTPoseRolloutFloater = newRolloutFloater "TPose输出FBX" 200 120
addrollout BipedTPoseRollout BipedTPoseRolloutFloater

-- FBX可以用Merge操作合并回Max