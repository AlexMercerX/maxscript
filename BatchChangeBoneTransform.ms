
rollout Common_BatchChangeBoneTransform "骨骼位置记录与应用"
(
    struct RecordBoneNameAndPosition (name, position)

    label lblRecordBoneNames "需记录的骨骼名 用@做分割"
    editText textRecordBoneNames "" align:#left width:140 tooltip:"填写需记录的骨骼名 用@做分割"
    button btnRecordSelectedParentSpacePosition "记录"
    button btnApplySelectedParentSpacePosition "应用"

    label lblFolderText "文件夹路径 (如D:\\Anim)"
    editText textFolderPath "" align:#left width:140 tooltip:"填写批量处理文件夹的路径名，例如D:\\Anim"
    button btnApplyBatchSelectedParentSpacePosition "批量应用"

	local folderPath = ""
    local recordBoneNames = #()
    local recordBoneTransforms = #()
    local recordBoneData = #()

	on textFolderPath entered text do
	(
		folderPath = textFolderPath.text
	)

    on btnRecordSelectedParentSpacePosition pressed do
    (
        recordBoneData = #()

        recordBoneNames = filterString textRecordBoneNames.text "@"
        recordBoneTransforms = #()

        for curName in recordBoneNames do
        (
            local curBone = getNodeByName curName
            local curBonePos = undefined

            if curBone != undefined then
            (
                curBonePos = (in coordsys parent curBone.position)
                local boneData = RecordBoneNameAndPosition name:curName position:curBonePos

                append recordBoneData boneData
            )
        )

        messageBox "记录成功         "
        print recordBoneData
    )

    on btnApplySelectedParentSpacePosition pressed do
    (
        sliderTime = animationRange.start
        with animate off
        (
            for data in recordBoneData do
            (
                local boneName = data.name
                local bonePosition = data.position

                local boneNode = getNodeByName boneName
                if boneNode != undefined then
                (
                    (in coordsys parent boneNode.position = bonePosition)
                )
            )
        )
    )

    on btnApplyBatchSelectedParentSpacePosition pressed do
    (
		if folderPath == "" then
        (
			messagebox("文件夹路径未设置")
            return false
        )

        for f in getfiles (folderPath + "\\*.max") do 
        (
            print ("Open Max File:" + f as string)

            loadMaxFile f usefileunits:true

            sliderTime = animationRange.start
            with animate off
            (
                for data in recordBoneData do
                (
                    local boneName = data.name
                    local bonePosition = data.position

                    local boneNode = getNodeByName boneName
                    if boneNode != undefined then
                    (
                        (in coordsys parent boneNode.position = bonePosition)
                    )
                )
            )

            saveMaxFile f
        )
    )
)

addRollout Common_BatchChangeBoneTransform rolledUp:false