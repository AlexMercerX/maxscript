
with redraw off

rollout Main "AnimationUpdataBatch" width:284 height:173
(
	label lbl1 "1    打开TPose的Max文件" pos:[30,10]
	label lbl2 "2 " pos:[30,40]
	label lbl3 "3 " pos:[30,80]
	button btn_rig "Batch" pos:[80,73] width:120 height:30 
	edittext Text_1 "" pos:[45,38] fieldWidth:160 text:"请选择动捕数据源文件目录" 
	button MCDataFilePath "Select" pos:[215,38] width:50 height:18
	
	fn getFilesRecursive root pattern =
	(
		local dir_array = GetDirectories (root+"/*")
		for d in dir_array do
			join dir_array (GetDirectories (d+"/*"))
		local my_files = getFiles (root+"/*")
		for f in dir_array do
			join my_files (getFiles (f + pattern))
		return my_files
	)
	
	on btn_rig pressed do
	(
		try
		(
			--maxfilepath 	--当前打开的文件的文件路径
			--maxfilename  	--当前打开的文件名
			RigFilePath = maxfilepath + maxfilename							--获取当前打开文件的绝对路径
			local fbxfilearray = getFilesRecursive Text_1.text "*.fbx"		--获取文件夹内所有fbx
			local filecount = fbxfilearray.count							--统计fbx数量
			pluginManager.loadClass FbxImporter
			for i = 1 to filecount do 
			(
				local exportFilePath = substituteString fbxfilearray[i] ".fbx" ".max"
				FBXImporterSetParam "Mode" #merge
				FBXImporterSetParam "Animation" true
				FBXImporterSetParam "FillTimeline" true
				FBXImporterSetParam "BakeAnimationLayers" true
				FBXImporterSetParam "PointCache" true
				loadMaxFile RigFilePath useFileUnits:true quiet:true #noPrompt    --打开rig文件
				importFile fbxfilearray[i] #noPrompt
				saveMaxFile exportFilePath
				resetMaxFile #noprompt		--重置maxfile
			)
			messagebox("转换完成！      ")
		)
		catch();
	)
	
	--获取motion data 目录
	on MCDataFilePath pressed do
	(
		global tp_MaxfileArray=#()
		tp_Batch = getSavePath caption:"请选择动捕数据源文件目录"
		Text_1.text = tp_Batch 
		tp_MaxfileArray = getfiles (tp_Batch+"\*.fbx" as string)
		tp_Direcories = getDirectories (tp_Batch+"\*" as string)
		for i in tp_Direcories do
		(
			tp_MaxfileArrayFoo = getfiles (i+"*.fbx" as string)
			for a in tp_MaxfileArrayFoo do append tp_MaxfileArray a
		)
	)
)

createDialog Main