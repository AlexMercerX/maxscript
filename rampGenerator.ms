fn createRampByAxis axis uvmapchannel=
(
    convertToMesh $
    $.showVertexColors = true

    max modify mode
    addModifier $ (Unwrap_UVW())
    unwrapModifier = $.unwrap_uvw
    unwrapModifier.setMapChannel uvmapchannel
    unwrapModifier.reset()
    unwrapModifier.setTVSubObjectMode 3 -- Face
    unwrapModifier.setVC true -- Vertex Colors Channel
    max select all

    if axis == "u" or axis == "v" then
    (
        redVColors = #()
        addModifier $ (vertexPaint())
        numColorVerts = getNumCPVVerts $

        for index = 1 to numColorVerts do
        (
            vertColor = getvertcolor $ index
            if axis == "u" then append redVColors (color vertColor.red 0 0)
            else append redVColors (color vertColor.green 0 0)
        )
    )
    else
    (
        unwrapModifier.mappingMode 1 -- plane
        -- plane direction: x=0, y=1, z=2

        if axis == "z" then axisNum = 0
        else axisNum = 2

        unwrapModifier.mappingAlign axisNum

        redVColors = #()

        addModifier $ (vertexPaint())
        numColorVerts = getNumCPVVerts $

        for index = 1 to numColorVerts do
        (
            vertColor = getvertcolor $ index
            if axis == "x" then append redVColors (color vertColor.red 0 0)
            else append redVColors (color vertColor.green 0 0)
        )
    )

    deleteModifier $ unwrapModifier
    maxOps.CollapseNodeTo $ 1 true
    convertToMesh $

    for index = 1 to numColorVerts do
    (
        setVertColor $ index (redVColors[index])
    )

    addModifier $ (VertexPaint())

    /*
    addModifier $ (VertexPaint())
    vertexPaintModifier = $.VertexPaint
    vpt = VertexPaintTool()
    vpt.paintColor = color 0 255 0
    vpt.brushOpacity = 100
    vertexPaintModifier.layerMode = "Subtract"
    AssignVertexColors.ApplyNodes $ vertexPaint:vpt
    */
)


fn ReInitVertColor = (
    convertToMesh $
    $.showVertexColors = true
    numColorVerts = getNumCPVVerts $
    for index = 1 to numColorVerts do(
        setVertColor $ index (color 255 255 255)
    )
)


try (closeRolloutFloater RampRolloutFloater) catch()

rollout rolloutRampGenerator "根据轴向生成 Ramp 工具"
(
    radiobuttons axisRadioButton ""
    labels:#("x", "y", "z", "u", "v") columns:5 offset:[0,5] align:#center
    offsets:#([15,0], [15,0], [15,0], [15,0], [15,0]) default:1
	dropDownList mapChannel "选择MapChannel" offset:[120,0] width:100 items:#("1","2","3","4","5","6")
    button generateRampButton "生成 Ramp" toolTip:"选择一个模型，生成Ramp" offset:[75,10]
    button ReInitButton "复原顶点色" toolTip:"将选中模型顶点色全部刷白" offset:[-75,-26]


	on generateRampButton pressed do
	(	uvMapChannel = mapChannel.selected as integer
        if ( $ != undefined ) then
        (
            case of
            (
                (axisRadioButton.State == 1): (axis = "x")
                (axisRadioButton.State == 2): (axis = "y")
                (axisRadioButton.State == 3): (axis = "z")
                (axisRadioButton.State == 4): (axis = "u")
                (axisRadioButton.State == 5): (axis = "v")
            )
            createRampByAxis axis uvMapChannel
        )
        else messageBox("请选择一个模型!   ")
	)

    on ReInitButton pressed do(
        ReInitVertColor()
    )
)

RampRolloutFloater = newrolloutfloater "Ramp 生成工具" 250 140
addrollout rolloutRampGenerator RampRolloutFloater
-- addrollout rolloutRampGenerator rolledUp:true

