
rollout CheckLODRollout "检查FBX面数"
(
    button btnExeuteCheckFBXMesh "检查场景" width:200 height:20
    dotNetControl 'LODCheckList' "System.Windows.Forms.ListView" pos:[5,30] width:390 height:200 align:#center
    -- listview 说明
    -- https://learn.microsoft.com/en-us/dotnet/api/system.windows.forms.listview?view=windowsdesktop-8.0

    on CheckLODRollout open do
	(
		--设置ListView的属性
		LODCheckList.Fullrowselect = true
		LODCheckList.GridLines = false
	    LODCheckList.View = LODCheckList.View.Details
	    
		--添加标题
	    LODCheckList.Columns.Add("Mesh")
	    LODCheckList.Columns.Add("NumFaces")
        LODCheckList.Columns.Add("NumVerts")
	)

    on btnExeuteCheckFBXMesh pressed do
    (
        LODCheckList.BeginUpdate()   --数据更新，UI暂时挂起，直到EndUpdate绘制控件，可以有效避免闪烁并大大提高加载速度
        
        LODCheckList.Clear()

		--添加标题
	    LODCheckList.Columns.Add("Mesh")
	    LODCheckList.Columns.Add("Faces")
        LODCheckList.Columns.Add("Verts")

        -- LODCheckList.Columns[0].Width = -1
        -- LODCheckList.Columns[1].Width = -1
        -- LODCheckList.Columns[2].Width = -1

        local totalFaceNum = 0
        local totalVertNum = 0

        for geo in $geometry do
        (
            local meshName = geo.name
            local meshNumFaces = geo.mesh.numfaces
            local meshNumVerts = geo.mesh.numverts

            local newRow = dotNetObject "System.Windows.Forms.ListViewItem"
	        newRow.Text = meshName
            newRow.SubItems.Add(meshNumFaces as string)
            newRow.SubItems.Add(meshNumVerts as string)
	        LODCheckList.Items.Add(newRow)

            TotalFaces = totalFaceNum + geo.mesh.numfaces
            TotalVerts = totalVertNum + geo.mesh.numverts

            print(newRow.Text)
            print(meshNumFaces)
            print(meshNumVerts)
        )
        
        -- Total
        local newRow = dotNetObject "System.Windows.Forms.ListViewItem"
        newRow.Text = "Total"
        newRow.SubItems.Add(totalFaceNum as string)
        newRow.SubItems.Add(totalVertNum as string)
        LODCheckList.Items.Add(newRow)

        LODCheckList.EndUpdate()   --数据更新，UI暂时挂起，直到EndUpdate绘制控件，可以有效避免闪烁并大大提高加载速度
    )

)

rollout BatchLODRollout "批量FBX减面"
(
    button btnSingleExecute "单个执行"
    button btnBatchExecute "批量执行"
    label lbl "FBX目录" align:#left across:2
    editText edtFileRoot "" align:#right width:100    

    local fileRoot = ""

    fn ReduceMeshNum curNode TargetFaceRatio = 
    (
        modPanel.addModToSelection (Polygon_Cruncher ()) ui:on

        -- Settings
        -- $.modifiers[#Polygon_Cruncher].KeepUV = on
        -- $.modifiers[#Polygon_Cruncher].KeepNormals = on

        $.modifiers[#Polygon_Cruncher].Calculate = on
        $.modifiers[#Polygon_Cruncher].vertexPercent = TargetFaceRatio*100
        maxOps.CollapseNode $ true
    )

    fn SetFBXExportParams = 
    (
        FbxExporterSetParam "Animation" false

        fbxExporterSetParam "FileVersion" "FBX201600"
		fbxExporterSetParam "ASCII" false

		fbxExporterSetParam "SmoothingGroups" true
		fbxExporterSetParam "NormalsPerPoly" false
		fbxExporterSetParam "TangentSpaceExport" true
		fbxExporterSetParam "GeomAsBone" true
		fbxExporterSetParam "Triangulate" true
		fbxExporterSetParam "PreserveEdgeOrientation" false

		fbxExporterSetParam "ColladaTriangulate" false
		fbxExporterSetParam "ColladaSingleMatrix" false
		fbxExporterSetParam "ColladaFrameRate" 60.f

		fbxExporterSetParam "Cameras" false
		fbxExporterSetParam "Lights" false

		fbxExporterSetParam "EmbedTextures" false
		fbxExporterSetParam "UpAxis" "Y"

		fbxExporterSetParam "Convert2Tiff" false
		fbxExporterSetParam "ConvertUnit" "cm"
		fbxExporterSetParam "FilterKeyReducer" true
		fbxExporterSetParam "GenerateLog" false
		fbxExporterSetParam "PointCache" false
		fbxExporterSetParam "PreserveInstances" false
		fbxExporterSetParam "RemoveSingleKeys" true
		fbxExporterSetParam "ScaleFactor" 1.0
		fbxExporterSetParam "SelectionSetExport" false
		fbxExporterSetParam "ShowWarnings" true
		fbxExporterSetParam "SmoothMeshExport" false
		fbxExporterSetParam "UseSceneName" false
    )

    on edtFileRoot entered text do
    (
        fileRoot = edtFileRoot.text
    )

    on btnBatchExecute pressed do
    (
        local targetFaceNum = 2900
        local totalFaceNum = 0

        setquietmode true
        SetFBXExportParams()

        for f in getfiles (fileRoot + "\\*.fbx") do 
		(
            resetMaxFile #noprompt
            importFile f #noprompt using:FBXIMP

            totalFaceNum = 0
            for geo in $geometry do
            (
                totalFaceNum = totalFaceNum + geo.mesh.numfaces
            )

            local reduceRatio = ((Float)targetFaceNum) / ((Float)totalFaceNum)
            print(f + " " + (reduceRatio as string))

            for geo in $geometry do
            (
                clearSelection()
                select geo
                max modify mode
                ReduceMeshNum geo reduceRatio
            )

            max select all
            exportFile f #noprompt selectedOnly:true using:FBXEXP
		)
			
		setquietmode false
    )

    on btnSingleExecute pressed do
    (
        local targetFaceNum = 6000
        local minRatio = 0.5
        local maxFacePerSubMesh = 2900

        local totalFaceNum = 0

        for geo in $geometry do
        (
            totalFaceNum = totalFaceNum + geo.mesh.numfaces
        )

        local generalReduceRatio = ((Float)targetFaceNum) / ((Float)totalFaceNum)
        if generalReduceRatio < minRatio then
        (
            generalReduceRatio = minRatio
        )
        
        print("General Reduce Ratio is " + generalReduceRatio as string)

        for geo in $geometry do
        (
            local realRatio = generalReduceRatio
            local maxRatio = ((Float)maxFacePerSubMesh) / ((Float)geo.mesh.numfaces)

            if realRatio > maxRatio then
            (
                realRatio = maxRatio
            )

            print(geo.name + " Reduce Ratio is " + realRatio as string)

            clearSelection()
            select geo
            max modify mode
            ReduceMeshNum geo realRatio
        )
    )
)

RootRolloutFloater = newRolloutFloater "FBX面数工具" 400 500
-- RootRolloutFloater = CreateDialog "FBX面数工具" 200 100
addrollout CheckLODRollout RootRolloutFloater
addrollout BatchLODRollout RootRolloutFloater