for i = animationRange.start to animationRange.end by 1 do
(
    at time i
    (
        quattoeuler (in coordsys parent Biped.getTransform $ #rotation)
    )
)