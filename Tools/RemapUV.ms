fn getFilesRecursive root pattern =
(
    local dir_array = GetDirectories (root+"/*")
    for d in dir_array do
        join dir_array (GetDirectories (d+"/*"))
    local my_files = getFiles (root+"/*")
    for f in dir_array do
        join my_files (getFiles (f + pattern))
    return my_files
)

fn MoveUVCoord SelNode = 
(
    if not (isKindOf SelNode Editable_mesh) then
    (
        convertToMesh SelNode
    )
    
    local NumUVS = ((meshOp.getNumMaps SelNode) - 1)

    for i = NumUVS to 2 by -1 do
    (
        channelInfo.ClearChannel SelNode i
    )

    NumUVS = ((meshOp.getNumMaps SelNode) - 1)

    if NumUVS == 1 then
    (
        channelInfo.AddChannel SelNode

        -- channelInfo.CopyChannel SelNode 3 2
        -- ChannelInfo.PasteChannel SelNode 3 3
        -- ChannelInfo.NameChannel SelNode 3 3 "LightMapUV"

        channelInfo.CopyChannel SelNode 3 1
        ChannelInfo.PasteChannel SelNode 3 2
        ChannelInfo.NameChannel SelNode 3 2 "UVmap_1"

        ChannelInfo.NameChannel SelNode 3 1 "UVmap_0"

        local UVWMapModi = (Uvwmap ())
        UVWMapModi.maptype = 0
        addmodifier SelNode UVWMapModi

        convertToMesh SelNode
    )
)

rollout ChangeUVCoordRollout "修改FBX导出参数"
(
    -- 填写fbx输出文件夹路径，格式参考 D:\\MyFolder\\Export，依据自己情况设置
    label lbl "FBX目录" align:#left across:2
    editText edtOutputRoot "" align:#right width:80
    
    button btnExeuct "执行" width:100
    button btnOpenTable "打开Channel表" width:100

    on btnExeuct pressed do
    (
        local fileDir = edtOutputRoot.text
        FBXFiles = getFilesRecursive fileDir "*.fbx"
        for FBXFile in FBXfiles do
        (
            print(FBXFile)
            FBXImporterSetParam "Mode" #create
            importFile FBXFile #noPrompt
            for SelNode in $geometry do
            (
                MoveUVCoord SelNode
            )
            exportFile FBXFile #noPrompt using:FBXEXP
            resetMaxFile #noPrompt
        )
    )

    on btnOpenTable pressed do
    (
        ChannelInfo.dialog()
    )
)

createDialog ChangeUVCoordRollout