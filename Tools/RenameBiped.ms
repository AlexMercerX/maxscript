
fn getFilesRecursive root pattern =
(
    local dir_array = GetDirectories (root+"/*")
    for d in dir_array do
        join dir_array (GetDirectories (d+"/*"))
    local my_files = getFiles (root+"/*")
    for f in dir_array do
        join my_files (getFiles (f + pattern))
    return my_files
)


fn ChangeBip001toBip01 = 
(
    $'Bip001'.name = "Bip01"
    $'Bip001 L Clavicle'.name = "Bip01 L Clavicle"
    $'Bip001 L UpperArm'.name = "Bip01 L UpperArm"
    $'Bip001 L Forearm'.name = "Bip01 L Forearm"
    $'Bip001 L Hand'.name = "Bip01 L Hand"
    $'Bip001 R Clavicle'.name = "Bip01 R Clavicle"
    $'Bip001 R UpperArm'.name = "Bip01 R UpperArm"
    $'Bip001 R Forearm'.name = "Bip01 R Forearm"
    $'Bip001 R Hand'.name = "Bip01 R Hand"
    $'Bip001 L Finger0'.name = "Bip01 L Finger0"
    $'Bip001 L Finger01'.name = "Bip01 L Finger01"
    $'Bip001 L Finger1'.name = "Bip01 L Finger1"
    $'Bip001 L Finger11'.name = "Bip01 L Finger11"
    $'Bip001 L Finger2'.name = "Bip01 L Finger2"
    $'Bip001 L Finger21'.name = "Bip01 L Finger21"
    $'Bip001 R Finger0'.name = "Bip01 R Finger0"
    $'Bip001 R Finger01'.name = "Bip01 R Finger01"
    $'Bip001 R Finger1'.name = "Bip01 R Finger1"
    $'Bip001 R Finger11'.name = "Bip01 R Finger11"
    $'Bip001 R Finger2'.name = "Bip01 R Finger2"
    $'Bip001 R Finger21'.name = "Bip01 R Finger21"
    $'Bip001 L Thigh'.name = "Bip01 L Thigh"
    $'Bip001 L Calf'.name = "Bip01 L Calf"
    $'Bip001 L HorseLink'.name = "Bip01 L HorseLink"
    $'Bip001 L Foot'.name = "Bip01 L Foot"
    $'Bip001 R Thigh'.name = "Bip01 R Thigh"
    $'Bip001 R Calf'.name = "Bip01 R Calf"
    $'Bip001 R HorseLink'.name = "Bip01 R HorseLink"
    $'Bip001 R Foot'.name = "Bip01 R Foot"
    $'Bip001 L Toe0'.name = "Bip01 L Toe0"
    $'Bip001 L Toe01'.name = "Bip01 L Toe01"
    $'Bip001 L Toe1'.name = "Bip01 L Toe1"
    $'Bip001 L Toe11'.name = "Bip01 L Toe11"
    $'Bip001 R Toe0'.name = "Bip01 R Toe0"
    $'Bip001 R Toe01'.name = "Bip01 R Toe01"
    $'Bip001 R Toe1'.name = "Bip01 R Toe1"
    $'Bip001 R Toe11'.name = "Bip01 R Toe11"
    $'Bip001 Spine'.name = "Bip01 Spine"
    $'Bip001 Spine1'.name = "Bip01 Spine1"
    $'Bip001 Spine2'.name = "Bip01 Spine2"
    $'Bip001 Head'.name = "Bip01 Head"
    $'Bip001 Pelvis'.name = "Bip01 Pelvis"
    $'Bip001 Footsteps'.name = "Bip01 Footsteps"
    $'Bip001 Neck'.name = "Bip01 Neck"
    $'Bip001 L ForeTwist'.name = "Bip01 L ForeTwist"
    $'Bip001 L ForeTwist1'.name = "Bip01 L ForeTwist1"
    $'Bip001 R ForeTwist'.name = "Bip01 R ForeTwist"
    $'Bip001 R ForeTwist1'.name = "Bip01 R ForeTwist1"
    $'Bip001 LUpArmTwist'.name = "Bip01 LUpArmTwist"
    $'Bip001 LUpArmTwist1'.name = "Bip01 LUpArmTwist1"
    $'Bip001 RUpArmTwist'.name = "Bip01 RUpArmTwist"
    $'Bip001 RUpArmTwist1'.name = "Bip01 RUpArmTwist1"
    $'Bip001 R Finger0Nub'.name = "Bip01 R Finger0Nub"
    $'Bip001 R Finger1Nub'.name = "Bip01 R Finger1Nub"
    $'Bip001 R Finger2Nub'.name = "Bip01 R Finger2Nub"
    $'Bip001 L Finger0Nub'.name = "Bip01 L Finger0Nub"
    $'Bip001 L Finger1Nub'.name = "Bip01 L Finger1Nub"
    $'Bip001 L Finger2Nub'.name = "Bip01 L Finger2Nub"
    $'Bip001 L Toe0Nub'.name = "Bip01 L Toe0Nub"
    $'Bip001 L Toe1Nub'.name = "Bip01 L Toe1Nub"
    $'Bip001 R Toe0Nub'.name = "Bip01 R Toe0Nub"
    $'Bip001 R Toe1Nub'.name = "Bip01 R Toe1Nub"
    $'Bip001 HeadNub'.name = "Bip01 HeadNub"
)

maxfiles = getFilesRecursive maxfilepath "*.max"

for f in maxfiles do
(
    loadMaxFile f usefileunits:true
    ChangeBip001toBip01()
    saveMaxFile f
)