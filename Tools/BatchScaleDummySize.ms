rollout AddRootOriginRollout "修改Dummy缩放"
(
    button btnAddRoot "添加"

    on btnAddRoot pressed do
    (
        for helper in helpers where (isKindOf helper Dummy) do
        (
            helper.boxSize = helper.boxSize * 0.1
        )
    )
)

AddRootOriginRolloutFloater = newRolloutFloater "修改Dummy缩放" 200 100
addrollout AddRootOriginRollout AddRootOriginRolloutFloater