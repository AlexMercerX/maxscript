rollout Common_WorkSpaceUtil "工作环境"
(
    local exportDir = undefined
    
	group ""
	(
		label lblWorkSpace "当前工作环境:" align:#center offset:[0, 1]
		radiobuttons radWorkSpace ""
		labels:#("Gate","古剑4") columns:2 offset:[0,5] align:#center
		offsets:#([0,0], [0,0]) default:2
	)
    
	fn refreshFBXExportPath = 
	(   
		if A2UFn_IsWorkSpace_Gate() then
		(
			radWorkSpace.state = 1
			lblWorkSpace.text = "当前工作环境: Gate"
			exportDir = getINISetting A2UConfigPath "FBXExport" "dir_gate"
		)
		else if A2UFn_IsWorkSpace_Gujian4() then
		(
			radWorkSpace.state = 2
			lblWorkSpace.text = "当前工作环境: 古剑4"
			exportDir = getINISetting A2UConfigPath "FBXExport" "dir_gujian4"
		)
	)

	on radWorkSpace changed state do
	(
		if state == 1 then
		(
			if A2UFn_SetWorkSpace_Gate() then
			(
				lblWorkSpace.text = "当前工作环境: Gate"
				messagebox("当前工作环境切换至 Gate   ")
			)
		)
		else if state == 2 then
		(
			if A2UFn_SetWorkSpace_Gujian4() then
			(
				lblWorkSpace.text = "当前工作环境: 古剑4"
				messagebox("当前工作环境切换至 古剑4    ")
			)
		)
        A2UFn_ResetRollouts()
	)

    on Common_WorkSpaceUtil open do
    (
		refreshFBXExportPath()
    )
)