
rollout Common_SelectFaceByUVW "依据UVW选面"
(
    /*
    group "选中修改器【UnwrapUVW】选择的面"
    (
        button btnSelectFaceByUVWModifier "选面" toolTip:"选中此模型UnwrapUVW修改器选择的面" width:70
    )
    */

    group "保存和读取选中的面"
    (
        button btnSaveFaceSelected "记录所选的面" across:2
        button btnLoadFaceSelected "选中记录的面"
        label lblSelectFaceNode "选中物体：无" offset:[0,2]
        label lblSelectFaceNum "选中面数：无"
        button btnClearSelection "清除记录" offset:[0,2]
    )
    local SelectedFaceNode = undefined
    local SelectedFaceBitArray = #{}

    fn GetFaceByUnwrapUVModifier obj = 
    (
        if (classOf obj.baseobject == Editable_Poly) and (obj.modifiers[#unwrap_uvw] != undefined) then
        (
            local selectFaceBitArray = obj.modifiers[#unwrap_uvw].getSelectedFacesByNode obj
            if selectFaceBitArray == undefined then
            (
                selectFaceBitArray = #{}
            )
            print("#### selectFaceBitArray")
            print selectFaceBitArray

            if selectFaceBitArray.count == 0 then
            (
                messageBox("未在UnwrapUVW中选择任何面")
            )
            else
            (
                max modify mode
                modPanel.setCurrentObject obj.baseObject
                subobjectLevel = 4
                obj.EditablePoly.SetSelection #Face selectFaceBitArray
                messageBox("已选中UnwrapUVW中选中的面      \n")
            )
        )
    )

    on btnSelectFaceByUVWModifier pressed do
    (
        if selection.count == 1 then
        (
            GetFaceByUnwrapUVModifier selection[1]
        )
        else if selection.count == 0 then
        (
            messagebox("请至少选中一个物体!      \n")
        )
        else
        (
            messagebox("请不要选中超过一个物体!      \n")
        )
    )

    on btnSaveFaceSelected pressed do
    (
        local obj = (selection as array)[1]
        if classof(modPanel.getCurrentObject()) == Unwrap_UVW then
        (
            if subobjectLevel == 3 then
            (
                local tempSelectFaceBitArray = obj.modifiers[#unwrap_uvw].getSelectedFacesByNode obj

                SelectedFaceNode = obj
                SelectedFaceBitArray = tempSelectFaceBitArray

                lblSelectFaceNode.text = "选中物体：" + SelectedFaceNode.name
                lblSelectFaceNum.text = "选中面数：" + SelectedFaceBitArray.numberSet as string
            )
            else
            (
                messageBox("请选中一个物体的【UnwrapUVW】修改器\n并切换到【面模式】 选中需要记录的面!                \n")
            )
        )
        else
        (
            messageBox("请选中一个物体的【UnwrapUVW】修改器\n并切换到【面模式】 选中需要记录的面!                \n")
        )
    )

    on btnLoadFaceSelected pressed do
    (
        if SelectedFaceNode == undefined then
        (
            messageBox("记录所选面的物体不存在        \n")
        )
        else
        (
            if SelectedFaceBitArray.numberset == 0 then
            (
                messageBox("没有记录任何选中的面        \n")
            )
            else
            (
                clearSelection()
                select SelectedFaceNode
                max modify mode
                modPanel.setCurrentObject SelectedFaceNode.baseObject
                subobjectLevel = 4
                SelectedFaceNode.EditablePoly.SetSelection #Face SelectedFaceBitArray
                messageBox("已选中记录选中的面        \n")
            )
        )
    )

    on btnClearSelection pressed do
    (
        SelectedFaceNode = undefined
        SelectedFaceBitArray = #{}
        messageBox("已清除选中记录        \n")
    )
)