-- Update Plugin
if (rollGenerateAnim != undefined) then
	removeRollout rollGenerateAnim
	
rollout rollGenerateAnim "动画批量导入导出"
(
	editText textSrcPath "源文件夹" align:#left width:140 tooltip:"填写源文件夹路径名，例如D:\\AnimOld"
	editText textDstPath "导出文件夹" align:#left width:140 tooltip:"填写导出文件夹路径名，例如D:\\AnimNew"
	editText textRefPath "导出参考文件" align:#left width:140 tooltip:"填写导出文件夹下的max文件名，例如anim.max"
    checkbox chkOutputBone "导出Bone" align:#left checked:true across:2
    checkbox chkMultiBiped "多套CS" align:#right checked:false tooltip:"多套CS骨骼，按照Root点匹配名字，分别导出.bip文件并导入"
	button btnGenerate "批量执行"
	
	local srcPath = ""
	local dstPath = ""
	local refPath = ""	
    local exportBone = true
    local multiBiped = false
	
	on textSrcPath entered text do
	(
		srcPath = textSrcPath.text
	)

	on textDstPath entered text do
	(
		dstPath = textDstPath.text
	)
	
	on textRefPath entered text do
	(
		refPath = textRefPath.text
	)

    on chkOutputBone changed theState do
    (
        exportBone = theState
    )

    on chkMultiBiped changed theState do
    (
        multiBiped = theState
    )

    fn getAllBipedObjects = 
    (
        local bipedObjects = #()
        for geo in geometry where (isKindOf geo Biped_Object) do
        (
	        append bipedObjects geo
        )
        return bipedObjects
    )

    fn getAllBoneGeometry = 
    (
        local boneObjects = #()
        for geo in geometry where (isKindOf geo BoneGeometry) do
        (
	        append boneObjects geo
        )
        return boneObjects
    )

    fn getAllShapes = 
    (
        return shapes
    )

    fn getAllHelpers = 
    (
        return helpers
    )

    fn exportBip curPath = 
    (
        local dir_array = GetDirectories (curPath+"*")
        for d in dir_array do
            exportBip d

        for f in getfiles (curPath + "\\*.max") do 
        (
            loadMaxFile f usefileunits:true
            local fileName = getFilenameFile maxFileName

            -- 隐藏所有非bone的Geometry
            hideByCategory.none() 
            hideByCategory.geometry = true
            max unhide all #noPrompt
            for i = 1 to GetNumberDisplayFilters() do
                SetDisplayFilter i false

            max select all
            local allBipObjects = getAllBipedObjects()
            local bipRoots = #()
            
            -- [Vertical_Horizontal_Turn] : Matrix3Controller
            -- This class represents the transform controller for the root object within the biped system. 
            for obj in allBipObjects do
            (
                if classof(obj.controller) == Vertical_Horizontal_Turn then
                    append bipRoots obj
            )
            
            if multiBiped == false and bipRoots.count > 1 then
            (
                messagebox ("导出失败！有多套CS骨骼，请勾选【多套CS】选项")
                return false
            )

            if bipRoots.count > 0 then
            (
                if multiBiped == false then
                (
                    local obj = bipRoots[1]
                    local bipFilePath = dstPath + "\\AnimFiles\\" + fileName + ".bip"
                    local controller = obj.controller
                    biped.saveBipFileSegment controller bipFilePath animationRange.start animationRange.end
                )
                else
                (
                    for obj in bipRoots do
                    (
                        local bipFilePath = dstPath + "\\AnimFiles\\" + fileName + obj.name + ".bip"
                        local controller = obj.controller
                        biped.saveBipFileSegment controller bipFilePath animationRange.start animationRange.end
                    )
                )
            )

            if exportBone == true and bipRoots.count > 0 then
            (
                local xafFilePath = dstPath + "\\AnimFiles\\" + fileName + ".xaf"
                select bipRoots[1]
                actionMan.executeAction 0 "40099"  --select similar
                actionMan.executeAction 0 "40044"  --select invert
                -- 防止有些bone选不中
                local boneObjects = getAllBoneGeometry()
                local helperObjects = getAllHelpers()
                local shapeObjects = getAllShapes()
                selectmore boneObjects
                selectmore helperObjects
                selectmore shapeObjects
                LoadSaveAnimation.saveAnimation xafFilePath $ #() #()
            )
            
            -- copy maxfile to dstPath
			refMaxFilePath = dstPath + "\\" + refPath
			generatedMaxFilePath = dstPath + "\\" + fileName + ".max"
			copyFile refMaxFilePath generatedMaxFilePath
        )
    )

    fn importBip = 
    (
        for f in getfiles (dstPath + "\\*.max") do 
        (
            loadMaxFile f usefileunits:true
            local fileName = getFilenameFile maxFileName

            --隐藏所有非bone的Geometry
            hideByCategory.none() 
            hideByCategory.geometry = true
            max unhide all #noPrompt
            for i = 1 to GetNumberDisplayFilters() do
                SetDisplayFilter i false
            
            local allBipObjects = getAllBipedObjects()
            local bipRoots = #()
            if multiBiped == false and bipRoots.count > 1 then
            (
                messagebox ("导出失败！有多套CS骨骼，请勾选【多套CS】选项")
                return false
            )

            -- [Vertical_Horizontal_Turn] : Matrix3Controller
            -- This class represents the transform controller for the root object within the biped system. 
            for o in allBipObjects do
            (
                if classof(o.controller) == Vertical_Horizontal_Turn then
                    append bipRoots o
            )

            if bipRoots.count > 0 then
            (
                if multiBiped == false then
                (
                    local obj = bipRoots[1]
                    local bipFilePath = dstPath + "\\AnimFiles\\" + fileName + ".bip"
                    local controller = obj.controller
                    biped.loadBipFile controller bipFilePath
                )
                else
                (
                    for obj in bipRoots do
                    (
                        local bipFilePath = dstPath + "\\AnimFiles\\" + fileName + obj.name + ".bip"
                        local controller = obj.controller
                        biped.loadBipFile controller bipFilePath
                    )
                )
            )
            
            if exportBone and bipRoots.count > 0 then
            (
                local xafFilePath = dstPath + "\\AnimFiles\\" + fileName + ".xaf"
                select bipRoots[1]
                actionMan.executeAction 0 "40099"  --select similar
                actionMan.executeAction 0 "40044"  --select invert
                -- 防止有些bone选不中
                local boneObjects = getAllBoneGeometry()
                local helperObjects = getAllHelpers()
                local shapeObjects = getAllShapes()
                selectmore boneObjects
                selectmore helperObjects
                selectmore shapeObjects
                LoadSaveAnimation.loadAnimation xafFilePath $ relative:false insert:false
            )
			saveMaxFile f
        )
    )
    
    on btnGenerate pressed do
	(
		if srcPath == "" then
			messagebox("外包路径未设置")
		else if dstPath == "" then
			messagebox("导出路径未设置")
		else if refPath == "" then
			messagebox("参考文件未设置")
        else if not isDirectoryWriteable dstPath then
            messagebox("导出路径不存在")
		else (
			local refFile = dstPath + "\\" + refPath
			if (not (doesFileExist refFile)) then
			(
				messagebox("参考文件不存在")
				return 0					
			)
			setquietmode true
            
			tmpPath = dstPath + "\\AnimFiles"
			makeDir tmpPath
			exportBip srcPath
			importBip()

			setquietmode false
		)
	)
)

addRollout rollGenerateAnim