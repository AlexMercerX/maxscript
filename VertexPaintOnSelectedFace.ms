rollout Common_VertexPaintToSelectedFace "给选中面刷顶点色"
(
    group "选择物体（在Poly层级选中处理的面）"
    (
        pickbutton pbtObjectPicker "选择物体" offset:[0,4]
        label lblObjectSelectFace "----"
    )
    group "Vertex Color"
    (
        label lblVertexColor "选择顶点色Color" across:2 align:#left
        colorpicker cpkVertexColor color:[255,255,255] modal:false align:#right
        button btnPaintSelectFaceVertexColor "刷顶点色Color"
    )
    group "Vertex Alpha"
    (
        label lblVertexAlpha "选择顶点色Alpha(0~1)" across:2 align:#left
        spinner spnVertexAlpha range:[0,1,0] type:#float align:#right
        button btnPaintSelectFaceVertexAlpha "刷顶点色Alpha"
    )
    local newVertexColor = [1,1,1]
    local newVertexAlpha = 1

	fn AddVertPaintModifier_SetVertColor curNode paintColor = 
	(
		max modify mode
		modPanel.setCurrentObject curNode.baseObject
		local vpModifier = VertexPaint()
		modPanel.addModToSelection (vpModifier)
		local CollpaseID = curNode.modifiers.count
		maxOps.CollapseNodeTo curNode CollpaseID true
		local curVerts = (polyop.getNumVerts curNode)
		polyop.setVertColor curNode 0 #{1 .. curVerts} paintColor
	)
	
	fn AddVertPaintModifier_SetVertAlpha curNode paintAlpha = 
	(
		max modify mode
		modPanel.setCurrentObject curNode.baseObject
		local vpModifier = VertexPaint()
		vpModifier.mapChannel = -2
		modPanel.addModToSelection (vpModifier)
		local CollpaseID = curNode.modifiers.count
		maxOps.CollapseNodeTo curNode CollpaseID true
		local curVerts = (polyop.getNumMapVerts curNode -2)
		for i = 1 to curVerts do
		(
			polyop.setMapVert curNode -2 i [paintAlpha,paintAlpha,paintAlpha]
		)
	)

    fn SetVertexColorToSelectFaces obj faceBitArray paintColor =
    (
        if not (polyop.getMapSupport obj 0) then
        (
            AddVertPaintModifier_SetVertColor obj (color 255 255 255)
        )

        local vertColorNumFaces = polyop.getNumMapFaces obj 0
        local vertColorNumVerts = polyop.getNumMapVerts obj 0
        local vertColorToPaintBitArray = #{}

        for i = 1 to vertColorNumFaces do
        (
            if faceBitArray[i] == true then
            (
                local vertArray = polyop.getMapFace obj 0 i
                for id in vertArray do
                (
                    vertColorToPaintBitArray[id] = true
                )
            )
        )

        for i = 1 to vertColorNumVerts do
        (
            if vertColorToPaintBitArray[i] == true then
            (
                polyop.SetMapVert obj 0 i paintColor
            )
        )
    )

    fn SetVertexAlphaToSelectFaces obj faceBitArray alpha =
    (
        if not (polyop.getMapSupport obj -2) then
        (
            AddVertPaintModifier_SetVertAlpha obj 1.0
        )

        local vertAlphaNumFaces = polyop.getNumMapFaces obj -2
        local vertAlphaNumVerts = polyop.getNumMapVerts obj -2
        local vertAlphaToPaintBitArray = #{}

        for i = 1 to vertAlphaNumFaces do
        (
            if faceBitArray[i] == true then
            (
                local vertArray = polyop.getMapFace obj -2 i
                for id in vertArray do
                (
                    vertAlphaToPaintBitArray[id] = true
                )
            )
        )

        for i = 1 to vertAlphaNumVerts do
        (
            if vertAlphaToPaintBitArray[i] == true then
            (
                polyop.SetMapVert obj -2 i alpha
            )
        )
    )

    on btnPaintSelectFaceVertexColor pressed do
    (
        local obj = pbtObjectPicker.object
        if obj != undefined and classOf(obj.baseObject) == Editable_Poly then
        (
            local faceBitArray = polyop.getFaceSelection obj.baseObject
            if faceBitArray.numberSet == 0 then
            (
                faceBitArray = #{1..obj.numfaces}	
            )
            SetVertexColorToSelectFaces obj faceBitArray newVertexColor

            select obj
            obj.showVertexColors = on
            obj.vertexColorType = 0
            obj.vertexColorsShaded = off
            pbtObjectPicker.object = undefined
            pbtObjectPicker.text = "选择物体"
            lblObjectSelectFace.text = "----"
            messagebox("顶点色Color设置完成        \n")
        )
        else
        (
            messagebox("请先选择一个操作物体        \n")
        )

        /*
        local vp = VertexPaint()
        addmodifier obj vp
        */

    )

    on btnPaintSelectFaceVertexAlpha pressed do
    (
        local obj = pbtObjectPicker.object
        if obj != undefined and classOf(obj.baseObject) == Editable_Poly then
        (
            local faceBitArray = polyop.getFaceSelection obj.baseObject
            if faceBitArray.numberSet == 0 then
            (
                faceBitArray = #{1..obj.numfaces}	
            )
            SetVertexAlphaToSelectFaces obj faceBitArray [newVertexAlpha,newVertexAlpha,newVertexAlpha]
            
            select obj
            obj.showVertexColors = on
            obj.vertexColorType = 2
            obj.vertexColorsShaded = off
            pbtObjectPicker.object = undefined
            pbtObjectPicker.text = "选择物体"
            lblObjectSelectFace.text = "----"
            messagebox("顶点色Alpha设置完成        \n")
        )
        else
        (
            messagebox("请先选择一个操作物体        \n")
        )

        /*
        local vp = VertexPaint()
        addmodifier obj vp
        */
        
    )

    on pbtObjectPicker picked obj do
    (
		if obj != undefined then
        (
            if classOf(obj.baseObject) == Editable_Poly then
            (
                pbtObjectPicker.text = obj.name
                local tempTotalFaceNum = polyop.getNumfaces obj
                local tempSelectFaceBitArray = polyop.getFaceSelection obj.baseObject
                
                if tempSelectFaceBitArray.numberSet == 0 then
                (
                    lblObjectSelectFace.text = "将设置 所有面"
                )
                else if tempSelectFaceBitArray.numberSet == tempTotalFaceNum then
                (
                    lblObjectSelectFace.text = "将设置 所有面"
                )
                else
                (
                    lblObjectSelectFace.text = "将设置 部分选中面"
                )
            )
            else
            (
                messageBox("需要选择一个Edit_Polygon物体")
            )
        )
    )

    on cpkVertexColor changed col do
    (
        newVertexColor = [col.r/255, col.g/255, col.b/255]

        print("color")
        print(newVertexColor)
    )

    on spnVertexAlpha changed val do
    (
        newVertexAlpha = val
        if newVertexAlpha > 1 then newVertexAlpha = 1
        if newVertexAlpha < 0 then newVertexAlpha = 0

        print("alpha")
        print(newVertexAlpha)
    )
)

/*
-- 加载逻辑转移到ShareTools.ms中
if Common_VertexPaintToSelectedFace != undefined then
(
    DestroyDialog Common_VertexPaintToSelectedFace
)
createDialog Common_VertexPaintToSelectedFace width:250
*/

/*
polyop.getFaceSelection $
polyop.getMapSupport $ 1
true
polyop.getNumFaces $
512
polyop.getNumMapFaces $ 1
512
polyop.getNumMapVerts $ 1
559
polyop.getMapFace $ 1 512
#(559, 527, 526)
polyop.getMapVert $ 1 200
[0.0625,0.625,0]

rollout Common_SelectFaceByUVW "依据UVW选面"
(
    fn GetFacesByUVRange = 
    (
        local numVerts = polyop.getNumVerts polyNode
        local numMapVerts = polyop.getNumMapVerts polyNode 1

        local numFaces = polyop.getNumFaces polyNode
        local uvInvalidVertIDs = #{}
        local uvValidFaceIDs = #{}

        for i = 1 to numMapVerts do
        (
            local uvCoord = polyop.getMapVert polyNode 1 i
            if uvCoord.y < 0.07 then
            (
                uvInvalidVertIDs[i] = true
            )
        )

        for i = 1 to numFaces do
        (
            local vertIDs = polyop.getMapFace polyNode 1 i
            local bValid = true

            for vid in vertIDs do
            (
                if uvInvalidVertIDs[vid] == true then
                (
                    bValid = false
                    exit
                )
            )

            if bValid == true then
            (
                uvValidFaceIDs[i] = true
            )
        )
        
        print uvValidFaceIDs
        select polyNode

        max modify mode
        subobjectLevel = 4
        polyNode.EditablePoly.SetSelection #Face uvValidFaceIDs
        
        $Box001.modifiers[#unwrap_uvw].unwrap6.getSelectedFacesByNode $Box001
    )
)
*/