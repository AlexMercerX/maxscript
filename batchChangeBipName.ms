rollout batchChangeBipNamesRollout "检查CS骨骼前缀"
(
    editText edt_rootname "CS骨骼前缀" text:"Bip01" align:#left width:140 
    button btn_execute "执行" width:100 offset:[0,5]
    local rootname = "Bip01"

    fn getAllBipedObjects = 
    (
        local bipedObjects = #()
        for geo in geometry where (isKindOf geo Biped_Object) do
        (
	        append bipedObjects geo
        )
        return bipedObjects
    )

    fn getAllBipedRoots = 
    (
        local bipedObjects = getAllBipedObjects()
        local bipRoots = #()
        for geo in bipedObjects do
        (
            if classof(geo.controller) == Vertical_Horizontal_Turn then
                append bipRoots geo
        )
        return bipRoots
    )

    function batchChangeBipNames = 
    (
        setquietmode true
    
        print("********** CS骨骼质心 Bip01前缀检查开始 **********")
        
        for f in getfiles (maxFilePath + "\\*.max") do 
        (
            loadMaxFile f usefileunits:true
            
            local bipRootArray = getAllBipedRoots()
            if bipRootArray.count < 0 then
            (
                print((getFilenameFile f) + "  " + "失败！没有找到 CS骨骼质心")
            )
            else if bipRootArray.count > 1 then
            (
                print((getFilenameFile f) + "  " + "失败！找到 超过一个 CS骨骼质心")
            )
            
            local bipRoot = bipRootArray[1]
            local bipedObjects = getAllBipedObjects()
            for bipObj in bipedObjects do
            (
                if (findstring bipObj.name rootname == undefined) then
                (
                    print(("已修改前缀名:" + getFilenameFile f) + " 的 "+ bipObj.name)
                )
            )
            
            select bipRoot
            local bipController = bipRoot.controller
            bipController.rootName = rootname

            saveMaxFile f
        )
        
        print("********** CS骨骼质心 Bip01前缀检查结束 **********")
        setquietmode false
    )

    on edt_rootname entered text do
    (
        rootname = edt_rootname.text
    )

    on btn_execute pressed do
    (
        batchChangeBipNames()
    )

)

CreateDialog batchChangeBipNamesRollout