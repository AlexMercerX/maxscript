max modify mode
set animate on
if cameras.count > 0 then
(
	for cam in cameras do
	(
		fieldOfViewController = cam.fieldOfView.controller
		keyCount = numkeys fieldOfViewController
		--keyCount = scCamera.position.controller.keys.count

		--scCamera.fov.controller = copy scCamera.fieldOfView.controller
		--scCamera.fov.track = copy scCamera.fieldOfView.track
		startTime = getkeytime cam.position.controller 1
		
		for i = 1 to keyCount do 
		(
			keyFrame = getkeytime fieldOfViewController i
			at time keyFrame
			(
				cam.fov = at time keyFrame cam.fieldOfView
			)
		)
	)
)
set animate off