-- scriptPath = "\\\\192.168.21.16\\ArtTools\\Max2016\\AurogonMaxScript\\"
scriptPath = "C:\\MaxScript\\"
A2UConfigPath = (getFilenamePath (getMAXIniFile())) + "A2Uconfig.ini"

fn LoadMaxScript scriptName = 
(
	filePath = scriptPath + scriptName
	filein filePath
)

-- Helpers And Tools
LoadMaxScript "dataUpdate.ms"
LoadMaxScript "workspaceHelper.ms"
LoadMaxScript "exportfbxHelper.ms"
LoadMaxScript "assembleTool.ms"
LoadMaxScript "NormalMapping.ms"
LoadMaxScript "SelectFaceByUnwrapUVW.ms"
LoadMaxScript "VertexPaintOnSelectedFace.ms"
LoadMaxScript "ShareTools.ms"		-- Load ShareTools Last

-- Rollouts
LoadMaxScript "workspaceUtil.ms"
LoadMaxScript "exportfbxutilGate.ms"
LoadMaxScript "exportfbxutilGujian4.ms"

A2UFn_AddWorkspaceRollout "Gate" Common_WorkSpaceUtil false
A2UFn_AddWorkspaceRollout "Gate" Gate_ExportFbxUtil false
A2UFn_AddWorkspaceRollout "Gate" Gate_VertexColorUtil true
A2UFn_AddWorkspaceRollout "Gate" Gate_AnimToolsUtil true
A2UFn_AddWorkspaceRollout "Gate" Gate_RootOriginModifyUtil true
A2UFn_AddWorkspaceRollout "Gate" Common_SelectFaceByUVWTool true
A2UFn_AddWorkspaceRollout "Gate" Common_VertexPaintToSelectedFaceTool true
A2UFn_AddWorkspaceRollout "Gate" Common_NormalMappingTool true
A2UFn_AddWorkspaceRollout "Gate" Common_RolloutSolveExportGimbalLock true
A2UFn_AddWorkspaceRollout "Gate" Common_AssembleTool true
A2UFn_AddWorkspaceRollout "Gate" Common_MocapBipedTool true
A2UFn_AddWorkspaceRollout "Gate" Common_BatchRenameSkelName true
A2UFn_AddWorkspaceRollout "Gate" Common_BatchImportExportAnim true
A2UFn_AddWorkspaceRollout "Gate" Common_DebugTool true

A2UFn_AddWorkspaceRollout "Gujian4" Common_WorkSpaceUtil false
A2UFn_AddWorkspaceRollout "Gujian4" Gujian4_ExportFbxUtil false
A2UFn_AddWorkspaceRollout "Gujian4" Gujian4_ClothUtil true
A2UFn_AddWorkspaceRollout "Gujian4" Gujian4_VertexColorUtil true
A2UFn_AddWorkspaceRollout "Gujian4" Gujian4_AnimToolsUtil true
A2UFn_AddWorkspaceRollout "Gujian4" Gujian4_RootOriginModifyUtil true
A2UFn_AddWorkspaceRollout "Gujian4" Common_SelectFaceByUVWTool true
A2UFn_AddWorkspaceRollout "Gujian4" Common_VertexPaintToSelectedFaceTool true
A2UFn_AddWorkspaceRollout "Gujian4" Common_NormalMappingTool true
A2UFn_AddWorkspaceRollout "Gujian4" Common_RolloutSolveExportGimbalLock true
A2UFn_AddWorkspaceRollout "Gujian4" Common_AssembleTool true
A2UFn_AddWorkspaceRollout "Gujian4" Common_MocapBipedTool true
A2UFn_AddWorkspaceRollout "Gujian4" Common_BatchImportExportAnim true
A2UFn_AddWorkspaceRollout "Gujian4" Common_DebugTool true

A2UFn_ResetRollouts()