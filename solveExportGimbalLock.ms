/*
逻辑：
填入报错骨骼
填入相关mesh

对报错骨骼 assume skin pose
-- 根据报错骨骼找mesh

for mesh
	mesh 缩放为 100
	extract skin data to mesh  新产生的mesh为 SkinData_原mesh名
	记录skin modifier上的boneList
	对skin modifier 做 collapse to
	新加skin modifier，加boneList
	传权重
	删掉 SkinData_原mesh名
*/


-- string1 = "-zhuozi"
-- string2 = replace string1 1 1 "$"

-- namesArray = for s in selection collect s.name

fn getSkinBones obj =
(
	max modify mode
	select obj
	
	if obj.modifiers[#Skin] != undefined do
	(
		skinBones = #()
		skinModifiers = getclassinstances Skin target:obj
		
		for s = 1 to skinModifiers.count where skinModifiers.count > 0 do
		(
			boneCount = skinOps.getnumberbones skinModifiers[s]
			for x = 1 to boneCount do
			(
				myBone = (skinOps.getBoneName skinModifiers[s] x 0)
				append skinBones (getNodeByName myBone)
			)
		)
		
		print skinBones
	)
	return skinBones
)


-- Not considering multi skin modifiers condition
fn reSkins objName =
(
	obj = getNodeByName objName
	print obj
	obj.scale = [1,1,1]
	
	-- refresh mesh size
	sliderTime = 1f
	sliderTime = 0f 
	
	skinUtils.ExtractSkinData obj
	
	max modify mode
	select obj
	
	if obj.modifiers[#Skin] != undefined do
	(
		skinBones = #()
		skinModifiers = getclassinstances Skin target:obj
		
		if skinModifiers.count > 1 then
		(	
			print skinModifiers.count
			print "Warning! Too many Skin modifiers!"
			return 0
		)
		else
		(	
			if skinModifiers.count == 1 do
			(
				boneCount = skinOps.getnumberbones skinModifiers[1]
				for i = 1 to boneCount do
				(
					myBone = (skinOps.getBoneName skinModifiers[1] i 0)
					append skinBones (getNodeByName myBone)
				)				
			)

			print skinBones

		)
		/*
		for modifierIndex = 1 to obj.modifiers.count do
		(	
			if obj.modifiers[modifierIndex] == skinModifiers[1] then 
			(
				skinModifiers[1].mirrorEnabled = off
				-- deleteModifier obj skinModifiers[1]
				maxOps.CollapseNodeTo obj modifierIndex off
			)
		)
		*/
		-- modPanel.setCurrentObject skinModifiers[1]
		modifierIndex = modPanel.getModifierIndex obj skinModifiers[1]
		maxOps.CollapseNodeTo obj modifierIndex off

		addModifier obj (Skin())
		newSkin = obj.modifiers[#Skin]
		
		skinDataObjName = "SkinData_" + objName
		skinDataObj = getNodeByName skinDataObjName
		
		for skinBoneIndex=1 to skinBones.count do
		(	
			skinBone = skinBones[skinBoneIndex]
			modPanel.setCurrentObject newSkin
			skinOps.addBone newSkin skinBone skinBoneIndex
			print skinBone
		)

		select #(obj, skinDataObj)
		skinUtils.ImportSkinDataNoDialog true false false false false 1.0 0
		-- newSkin.always_deform = false
		-- newSkin.enabled=true
		-- newSkin.always_deform = true
		delete skinDataObj
		clearSelection()
	)
)


fn batching boneArray meshArray=
(
	-- boneArray = #("huatong", "shanzi", "muban", "Bone001", "Bone003", "Bone005", "zhuozi")
	-- meshArray = #("SM_af32mrr_csdesk_01", "SK_af32mrr_default_CG")

	sliderTime = 0f

	for boneName in boneArray do
	(
		myBone = getNodeByName boneName
		myBone.assumeSkinPose()
		skinModifiers = getclassinstances Skin target:myBone
		for index = 1 to skinModifiers.count do deleteModifier myBone skinModifiers[index]
	)

	for meshName in meshArray do
	(
		reSkins meshName
	)
)


-- try (closeRolloutFloater RolloutFloaterTemplate) catch()

rollout rolloutSolveExportGimbalLock "导出FBX Gimbal Lock 修复工具"
(
	group ""
	(
		edittext boneList "骨骼/控制器:" height:100 labelOnTop:true
		listbox meshList "模型:" height:7 items:#() readOnly:false
		
		button addMeshButton "添加模型" toolTip:"将选择的模型添加到上面的列表中" align:#center across:2 offset:[0,2]
		button deleteMeshButton "删除模型" toolTip:"删除上面的列表中选择的模型" align:#center offset:[0,2]
	)
	
	button batchingButton "开始批处理" toolTip:"1.让骨骼/控制器回到初始Pose（Assume Skin Pose）\n2.模型还原缩放到100\n3.重做Skin，还原权重"
	
	on addMeshButton pressed do
	(	
		tempArray = meshList.items
		for obj in selection do
		(	
			objName = obj.name
			if (findItem tempArray objName) != 0 or (classOf obj as string) != "PolyMeshObject" do continue
			meshList.items = append meshList.items objName
		)
	)

	on deleteMeshButton pressed do
	(
		if meshList.items.count > 0 and meshList.selection > 0 do
		(
			meshList.items = deleteItem meshList.items meshList.selection
		)
	)

	on batchingButton pressed do
	(
		boneArray = FilterString boneList.text "- \r\n"
		meshArray = meshList.items
		batching boneArray meshArray
	)
)

-- RolloutFloaterTemplate = newrolloutfloater "test" 250 400
-- addrollout rolloutSolveExportGimbalLock RolloutFloaterTemplate
addrollout rolloutSolveExportGimbalLock rolledUp:true

