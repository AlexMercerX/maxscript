
rollout VertexColorUtil "顶点色工具"
(
	label lblVC_Bake_SM "烘焙顶点色（SM物件）" align:#left
	button btnVC_Bake_SM "执行" align:#center toolTip:"烘焙法线方向到顶点色RGB通道" width:90

	label lblVC_Bake_Color "快速刷顶点色:" align:#left offset:[0,6]
	button btnVC_Bake_White "白" align:#center toolTip:"刷顶点色为(255,255,255)" width:24 across:5 offset:[0,2]
	button btnVC_Bake_Red "红" align:#center toolTip:"刷顶点色为(255,0,0)" width:24 offset:[0,2]
	button btnVC_Bake_Green "绿" align:#center toolTip:"刷顶点色为(0,255,0)" width:24 offset:[0,2]
	button btnVC_Bake_Blue "蓝" align:#center toolTip:"刷顶点色为(0,0,255)" width:24 offset:[0,2]
	button btnVC_Bake_Black "黑" align:#center toolTip:"刷顶点色为(0,0,0)" width:24 offset:[0,2]

	fn CheckCanConvert curNode = 
	(
		-- 如果不是Polygon 则不允许操作
		if ClassOf curNode.baseObject != Editable_Poly then
		(
			messagebox("请先将" + curNode.name + "转换为Polygon     \n")
			return false
		)
		-- 如果有VertexPaint修改器 提示先塌陷修改器
		for m in curNode.modifiers where ClassOf m == VertexPaint do
		(
			messagebox("请先塌陷掉" + curNode.name + "的VertexPaint修改器     \n")
			return false
		)
		
		return true
	)
	
	fn AddVertPaintModifier_SetVertColor curNode paintColor = 
	(
		max modify mode
		modPanel.setCurrentObject curNode.baseObject
		modPanel.addModToSelection (VertexPaint ())
		local CollpaseID = curNode.modifiers.count
		maxOps.CollapseNodeTo curNode CollpaseID true
		local curVerts = (polyop.getNumVerts curNode)
		for i = 1 to curVerts do
		(
			polyop.setVertColor curNode 0 #{i} paintColor
		)
	)
	
	fn SetSelectNodesVertexColor paintColor = 
	(
		local arrSelect = getCurrentSelection()
		if arrSelect.count <= 0 then
		(
			messagebox("未选中任何物体！     ")
			return false
		)
		-- 检查所选Node是否符合条件
		for curNode in arrSelect do
		(
			local ret = CheckCanConvert curNode
			if ret != true then
			(
				return false
			)
		)
		-- 刷新顶点色
		for curNode in arrSelect do
		(
			AddVertPaintModifier_SetVertColor curNode paintColor
		)
		return true
	)
	
	fn SetVertexColorLocalAvrageNormal curNode = 
	(
		-- 如果没有CPV点就要先增加VertexPaint修改器 并将它塌陷掉
		if curNode.mesh.numcpvverts == 0 then
		(
			AddVertPaintModifier_SetVertColor curNode (color 255 255 255)
		)

		local curNormal = #()
		local curVerts = (polyop.getNumVerts curNode)
		for i = 1 to curVerts do
		(
			local tFace = polyop.getFacesUsingVert curNode #(i)
			tFace = tFace as Array
			local tempNormalTotal = [0, 0, 0]
			for j = 1 to tFace.count do
			(
				tempNormalTotal += normalize(polyop.getFaceNormal curNode tFace[j])
			)
			tempNormalTotal = normalize(tempNormalTotal / tFace.count)
			-- ue4中y方向和3dsmax中相反，所以加负号
			append curNormal (tempNormalTotal * [1, -1, 1])
		)
		
		for i = 1 to curVerts do
		(
			local dirColor = (curNormal[i] + [1,1,1]) / 2 * 255
			local colR = dirColor.x												-- R（0~255）
			local colG = dirColor.y												-- G（0~255）
			local colB = dirColor.z												-- B（0~255）
			
			polyop.setVertColor curNode 0 #{i} (color colR colG colB)
		)
	)
	
	on btnVC_Bake_SM pressed do
	(
		local arrSelect = getCurrentSelection()
		if arrSelect.count <= 0 then
		(
			messagebox("未选中任何物体！      ")
			return false
		)
		-- 检查所选Node是否符合条件
		for curNode in arrSelect do
		(
			local ret = CheckCanConvert curNode
			if ret != true then
			(
				return false
			)
		)
		-- 刷新顶点色
		for curNode in arrSelect do
		(
			SetVertexColorLocalAvrageNormal curNode
		)
		messagebox("烘焙SM物件顶点色完成!      ")
		return true
	)

	on btnVC_Bake_White pressed do
	(
		local ret = SetSelectNodesVertexColor (color 255 255 255)
		if ret == true then
			messagebox("重置顶点色为白色完成!      ")
	)

	on btnVC_Bake_Red pressed do
	(
		local ret = SetSelectNodesVertexColor (color 255 0 0)
		if ret == true then
			messagebox("重置顶点色为红色完成!      ")
	)

	on btnVC_Bake_Green pressed do
	(
		local ret = SetSelectNodesVertexColor (color 0 255 0)
		if ret == true then
			messagebox("重置顶点色为绿色完成!      ")
	)

	on btnVC_Bake_Blue pressed do
	(
		local ret = SetSelectNodesVertexColor (color 0 0 255)
		if ret == true then
			messagebox("重置顶点色为蓝色完成!      ")
	)

	on btnVC_Bake_Black pressed do
	(
		local ret = SetSelectNodesVertexColor (color 0 0 0)
		if ret == true then
			messagebox("重置顶点色为黑色完成!      ")
	)
	
)

addRollout VertexColorUtil 