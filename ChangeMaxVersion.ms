with redraw off
rollout Main "VersionConversion" width:284 height:173
(
	label lbl1 "1 打开需转版本的任何一个Max文件" pos:[45,10]
	label lbl2 "2 Version：" pos:[45,40]
	button btn_rig "Batch" pos:[55,73] width:180 height:30 
	edittext Text_1 "" pos:[115,38] fieldWidth:40  text:"2017" 
	label lbl9 "shoven" pos:[30,147] width:136 height:15

	fn getFilesRecursive root pattern =
	(
		dir_array = GetDirectories (root+"/*")
		for d in dir_array do
		  join dir_array (GetDirectories (d+"/*"))
		my_files = #()
		append dir_array (root + "\\")
		for f in dir_array do
		  join my_files (getFiles (f + pattern))
		my_files
	)

	on btn_rig pressed do
	(
		try
		(
			version1 = 1
			version1 = Text_1.text as integer
			RigFilePath=maxfilepath+maxfilename
			exportFilePath=maxfilepath + "VersionTo" + Text_1.text
			maxfilepath1=maxfilepath
			makeDir exportFilePath
			fbxfilearray = getFilesRecursive maxfilepath1 "*.max"
			filecount=fbxfilearray.count 	
			for i = 1 to filecount do (
				loadMaxFile  fbxfilearray[i] useFileUnits:true quiet:true #noPrompt    --打开rig文件
				exportFilePath1=exportFilePath+"\\"+ maxfilename
				saveMaxFile exportFilePath1 saveAsVersion:version1
				resetMaxFile #noprompt
			)
		)
		catch
		(
			messageBox "无法转换"
		)
	)
)
createDialog Main
