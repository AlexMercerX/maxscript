
with redraw off

rollout MocapBipedToolsRollout "MocapBipedTools" width:250 height:244
(
    group "添加Root/*Origin点"
    (
        label lblAddRoot "请选中骨架的质心点(如:Bip01)" align:#left width:190 across:2
        button btnAddRoot "添加" align:#right width:40
    )
    group "生成TPose Fbx"
    (
        editText textFBXSuffix "Fbx文件后缀" width:160 text:"_TPose" align:#left
        checkbox chkBipedOnly "只输出CS骨骼" default:false align:#Left
        label lblTPoseExport "输出Tpose Fbx到Max文件目录" width:180 align:#left across:2
        button btnTPoseExport "输出" width:40 align:#right
    )
    group "动捕数据应用到Max文件"
    (
        label lbl1 "1  打开需要应用动捕fbx的Max文件" align:#left
        label lbl2 "2" across:3 align:#left width:10 offset:[0,8]
        edittext MocapFbxPath "" width:160 offset:[-65,6] text:"请选择动捕fbx源文件目录"
        button btnSelectMocapFbxPath "选择" width:40 offset:[0,6] height:18 align:#right
        label lbl3 "3" across:2 align:#left width:10 offset:[0,6]
        button btnatchExecute "批处理转换" offset:[-100,0] width:100 align:#left toolTip:"根据所有动捕动作fbx，依次生成对应max文件（帧速率60FPS）"
    )
	
    fn TraversalChildNode parentNode selectionArray = 
    (
        if parentNode == undefined then
        (
            return false
        )

        for childnode in parentNode.children do
        (
            if childnode.children.count > 0 then
            (
                if not (TraversalChildNode childnode selectionArray) then
                    return false
            )
            else
            (
                append selectionArray childnode
            )
        )
        append selectionArray parentNode
        return true
    )

    fn SelectSkeletonAll = 
    (
        local selectionArray = #()
        local ret = TraversalChildNode $Root selectionArray
        if ret then
        (
            selectMore selectionArray
        )
        return ret
    )

    fn GetAllBipedObjects = 
    (
        local bipedObjects = #()
        for geo in geometry where (isKindOf geo Biped_Object) do
        (
	        append bipedObjects geo
        )
        return bipedObjects
    )

	fn getFilesRecursive root pattern =
	(
		local dir_array = GetDirectories (root+"/*")
		for d in dir_array do
			join dir_array (GetDirectories (d+"/*"))
		local my_files = getFiles (root+"/*")
		for f in dir_array do
			join my_files (getFiles (f + pattern))
		return my_files
	)
	
    on btnAddRoot pressed do
    (
        if $ != undefined then
        (
            if getnodebyname "Root" == undefined then
                Dummy pos:[0,0,0] name:"Root" boxSize:[20,20,20]
            if getnodebyname "*origin" == undefined then
                Dummy pos:[0,0,0] name:"*origin" boxSize:[10,10,10]
            
            $.parent = $Root
            messageBox("添加Root和*Origin点成功!    ")
        )
        else
        (
            messageBox("请选中骨架的质心点!    ")
        )
    )

    on btnTPoseExport pressed do
    (
        setquietmode true
        max hold

        max unhide all
        local filename = maxfilepath + maxfilename
        if filename == "" then
        (
            setquietmode false
            messagebox("操作失败 Max文件异常!          ")
            return false
        )

        fbxExporterSetParam "Animation" false
        sliderTime = 0

        local rootBone = $Root
        if undefined == rootBone then
        (
            setquietmode false
            messagebox("操作失败 骨架上没有Root点!          ")
            return false
        )

        local BipedBones = GetAllBipedObjects()

        local L_Clavicle = undefined
        local L_UpperArm = undefined
        local L_Forearm = undefined
        local L_Hand = undefined

        local R_Clavicle = undefined
        local R_UpperArm = undefined
        local R_Forearm = undefined
        local R_Hand = undefined

        local L_Finger0Arr = #()
        local L_Finger1Arr = #()
        local L_Finger2Arr = #()
        local L_Finger3Arr = #()
        local L_Finger4Arr = #()

        local R_Finger0Arr = #()
        local R_Finger1Arr = #()
        local R_Finger2Arr = #()
        local R_Finger3Arr = #()
        local R_Finger4Arr = #()

        for BoneNode in BipedBones do
        (
            if (findString BoneNode.name "L Clavicle" != undefined) then
                L_Clavicle = BoneNode
            if (findString BoneNode.name "L UpperArm" != undefined) then
                L_UpperArm = BoneNode
            if (findString BoneNode.name "L Forearm" != undefined) then
                L_Forearm = BoneNode
            if (findString BoneNode.name "L Hand" != undefined) then
                L_Hand = BoneNode
            if (findString BoneNode.name "L Finger0" != undefined) then
                append L_Finger0Arr BoneNode
            if (findString BoneNode.name "L Finger1" != undefined) then
                append L_Finger1Arr BoneNode
            if (findString BoneNode.name "L Finger2" != undefined) then
                append L_Finger2Arr BoneNode
            if (findString BoneNode.name "L Finger3" != undefined) then
                append L_Finger3Arr BoneNode
            if (findString BoneNode.name "L Finger4" != undefined) then
                append L_Finger4Arr BoneNode

            if (findString BoneNode.name "R Clavicle" != undefined) then
                R_Clavicle = BoneNode
            if (findString BoneNode.name "R UpperArm" != undefined) then
                R_UpperArm = BoneNode
            if (findString BoneNode.name "R Forearm" != undefined) then
                R_Forearm = BoneNode
            if (findString BoneNode.name "R Hand" != undefined) then
                R_Hand = BoneNode
            if (findString BoneNode.name "R Finger0" != undefined) then
                append R_Finger0Arr BoneNode
            if (findString BoneNode.name "R Finger1" != undefined) then
                append R_Finger1Arr BoneNode
            if (findString BoneNode.name "R Finger2" != undefined) then
                append R_Finger2Arr BoneNode
            if (findString BoneNode.name "R Finger3" != undefined) then
                append R_Finger3Arr BoneNode
            if (findString BoneNode.name "R Finger4" != undefined) then
                append R_Finger4Arr BoneNode
        )

        if L_Clavicle != undefined then
            biped.settransform L_Clavicle #rotation ((eulerAngles 0 0 0) as quat) false
        if L_UpperArm != undefined then
            biped.settransform L_UpperArm #rotation ((eulerAngles 0 0 0) as quat) false
        if L_Forearm != undefined then
            biped.settransform L_Forearm #rotation ((eulerAngles 0 0 0) as quat) false
        if L_Hand != undefined then
            biped.settransform L_Hand #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger0Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger1Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger2Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger3Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        for _bone in L_Finger4Arr do
            biped.settransform _bone #rotation ((eulerAngles -90 0 0) as quat) false
        
        if R_Clavicle != undefined then
            biped.settransform R_Clavicle #rotation ((eulerAngles 0 180 0) as quat) false
        if R_UpperArm != undefined then
            biped.settransform R_UpperArm #rotation ((eulerAngles 0 180 0) as quat) false
        if R_Forearm != undefined then
            biped.settransform R_Forearm #rotation ((eulerAngles 0 180 0) as quat) false
        if R_Hand != undefined then
            biped.settransform R_Hand #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger0Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger1Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger2Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger3Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        for _bone in R_Finger4Arr do
            biped.settransform _bone #rotation ((eulerAngles 90 180 0) as quat) false
        
        clearSelection()
        if chkBipedOnly.checked then
        (
            local allBiped = GetAllBipedObjects()
            selectmore allBiped
        )
        else
        (
            SelectSkeletonAll()
        )
        
        filename = substring filename 1 (filename.count-4)
        filename = filename + textFBXSuffix.text + ".fbx"
        local dir = getFilenamePath filename
        if not isDirectoryWriteable dir then makeDir dir all:true

        exportFile filename #noPrompt selectedOnly:true using:FBXEXP

        max fetch
        setquietmode false

        messagebox("成功导出Tpose FBX骨架！     ")
        return true
    )

    -- FBX 帧速率只能是30 ?
	on btnatchExecute pressed do
	(
		try
		(
			--maxfilepath 	--当前打开的文件的文件路径
			--maxfilename  	--当前打开的文件名
			RigFilePath = maxfilepath + maxfilename							--获取当前打开文件的绝对路径
			local fbxfilearray = getFilesRecursive MocapFbxPath.text "*.fbx"		--获取文件夹内所有fbx
			local filecount = fbxfilearray.count							--统计fbx数量
			pluginManager.loadClass FbxImporter
			for i = 1 to filecount do 
			(
				local exportFilePath = substituteString fbxfilearray[i] ".fbx" ".max"
				FBXImporterSetParam "Mode" #merge
				FBXImporterSetParam "Animation" true
				FBXImporterSetParam "FillTimeline" true
				FBXImporterSetParam "BakeAnimationLayers" true
				FBXImporterSetParam "PointCache" true
				loadMaxFile RigFilePath useFileUnits:true quiet:true #noPrompt    --打开rig文件
                frameRate = 30
				importFile fbxfilearray[i] #noPrompt
                frameRate = 60
				saveMaxFile exportFilePath
				resetMaxFile #noprompt		--重置maxfile
			)
			messagebox("全部转换完成！      ")
		)
		catch();
	)
	
	--获取motion data 目录
	on btnSelectMocapFbxPath pressed do
	(
		global tp_MaxfileArray=#()
		tp_Batch = getSavePath caption:"请选择动捕数据源文件目录"
        if tp_Batch == undefined then
        (
            return false
        )
		MocapFbxPath.text = tp_Batch 
		tp_MaxfileArray = getfiles (tp_Batch+"\*.fbx" as string)
		tp_Direcories = getDirectories (tp_Batch+"\*" as string)
		for i in tp_Direcories do
		(
			tp_MaxfileArrayFoo = getfiles (i+"*.fbx" as string)
			for a in tp_MaxfileArrayFoo do append tp_MaxfileArray a
		)
	)
)

createDialog MocapBipedToolsRollout