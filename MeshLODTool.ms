rollout MeshLODToolRollout "FBX工具"
(
    label lblFBXTool_FBXPath "批量检查FBX目录" offsets:[0, -10] across:2
    editText edtFBXTool_FBXPath ""

    group "检查FBX面数"
    (
        button btnCheckFBX_CheckSingle "当前场景检查" height:20 across:3
        button btnCheckFBX_CheckBatch "批量检查" height:20
        checkbox chkCheckFBX_OnlyShowFailed "仅统计 检查失败" checked:false

        label lblCheckFBX_MeshData "Mesh" width:220 align:#left offset:[5,0] across:3
        label lblCheckFBX_MeshFaces "Faces" width:60 align:#left offset:[110,0]
        label lblCheckFBX_MeshVerts "Verts" width:60 align:#left offset:[55,0]

        listbox lstCheckFBX_MeshData width:220 height:20 align:#left across:3
        listbox lstCheckFBX_MeshFaces width:60 height:20 align:#left offset:[105,0]
        listbox lstCheckFBX_MeshVerts width:60 height:20 align:#left offset:[50,0]
    )

    group "手动优化FBX面数"
    (
        editText edtOptMannalFBX_OpenFBXFile "FBX文件" across:4
        button btnOptMannualFBX_ImportFBX "导入"
        checkbox chkOptMannualFBX_ImportFBX "导入时检查" checked:true
        button btnOptMannualFBX_AddLODModifier "增加 减面修改器"

        button btnOptMannualFBX_ExportFBXOverride "导出（覆盖）" across:3
        button btnOptMannualFBX_ExportFBXSubFolder "导出（另存）"
        editText edtOptMannalFBX_ExportFBXSubFolder "另存子目录名" text:"Output"
    )

    local FBXToolFileRoot = ""

    local checkFBXOnlyShowCheckFailed = false
    local checkFBXMaxFaceNumSingleMesh = 3000
    local checkFBXMaxFaceNumTotalMesh = 15000

    local doCheckWhenImport = true

    local optFBXManualFBXFile = ""
    local optFBXManualFBXSubFolder = "Output"

    fn CheckFBXCleanListData = 
    (
        lstCheckFBX_MeshData.items = #()
        lstCheckFBX_MeshFaces.items = #()
        lstCheckFBX_MeshVerts.items = #()
    )

    fn CheckFBXGetMeshData fbxFileName bOnlyCheckFailed bAddToUIList &LogString = 
    (
        local totalFaceNum = 0
        local totalVertNum = 0
        local LogStringCurrent = ""
        
        -- Each
        for geo in $geometry do
        (
            local meshName = geo.name
            local meshNumFaces = geo.mesh.numfaces
            local meshNumVerts = geo.mesh.numverts

            local SingleMeshNumFaceCheckPass = (meshNumFaces <= checkFBXMaxFaceNumSingleMesh)

            if not (bOnlyCheckFailed and SingleMeshNumFaceCheckPass) then
            (
                if bAddToUIList then
                (
                    lstCheckFBX_MeshData.items = append lstCheckFBX_MeshData.items (meshName)
                    lstCheckFBX_MeshFaces.items = append lstCheckFBX_MeshFaces.items (meshNumFaces as string)
                    lstCheckFBX_MeshVerts.items = append lstCheckFBX_MeshVerts.items (meshNumVerts as string)
                )
                LogStringCurrent = LogStringCurrent + "Faces:" + (meshNumFaces as string) + "\tVerts:" + (meshNumVerts as string) + "\tMesh:" + meshName + "\n"
            )
            totalFaceNum = totalFaceNum + geo.mesh.numfaces
            totalVertNum = totalVertNum + geo.mesh.numverts
            print("Mesh:" + meshName + " Faces:" + (meshNumFaces as string) + " Verts:" + (meshNumVerts as string))
        )
        
        -- Total
        local TotalMeshNumFaceCheckPass = (totalFaceNum <= checkFBXMaxFaceNumTotalMesh)
        if not (bOnlyCheckFailed and TotalMeshNumFaceCheckPass) then
        (
            if bAddToUIList then
            (
                lstCheckFBX_MeshData.items = append lstCheckFBX_MeshData.items ("[Total]" + fbxFileName)
                lstCheckFBX_MeshFaces.items = append lstCheckFBX_MeshFaces.items (totalFaceNum as string)
                lstCheckFBX_MeshVerts.items = append lstCheckFBX_MeshVerts.items (totalVertNum as string)
            )
            LogStringCurrent = LogStringCurrent + "Faces:" + (totalFaceNum as string) + "\tVerts:" + (totalVertNum as string) + "\tMesh:Total" + "\n"
            print("Mesh:Total" + " Faces:" + (totalFaceNum as string) + " Verts:" + (totalVertNum as string))
        )
        
        if LogStringCurrent != "" then
        (
            LogStringCurrent = "---- FBX Name ----\n" + fbxFileName + ".fbx\n" + LogStringCurrent
            LogString = LogString + LogStringCurrent
        )

    )

    on MeshLODToolRollout open do
    (
        CheckFBXCleanListData()
    )

    on chkCheckFBX_OnlyShowFailed changed theState do
    (
        checkFBXOnlyShowCheckFailed = theState
    )

    on btnCheckFBX_CheckSingle pressed do
    (
        CheckFBXCleanListData()
        
        local logString = ""

        local fbxFileName = "Current Scene"
        CheckFBXGetMeshData fbxFileName checkFBXOnlyShowCheckFailed true &logString
        logString = trimRight logString

        local SingleExportRoot = "C:\\ArtToolsLog\\Max2016"
        Fn_ExportTxtFile SingleExportRoot "CheckFBX.txt" logString
    )

    on btnCheckFBX_CheckBatch pressed do
    (
        CheckFBXCleanListData()

        setquietmode true

        Fn_SetDefaultFBXImportSetting "model"
        local logString = ""
        
        for f in getfiles (FBXToolFileRoot + "\\*.fbx") do
        (
            resetMaxFile #noprompt
            importFile f #noprompt using:FBXIMP
            local fbxFileName = getFilenameFile f 
            CheckFBXGetMeshData fbxFileName checkFBXOnlyShowCheckFailed false &logString
        )
        
        setquietmode false

        logString = trimRight logString
        Fn_ExportTxtFile FBXToolFileRoot "CheckFBX.txt" logString
    )

    fn SelectListCheckSelectedFBXMesh = 
    (
        local nodeName = lstCheckFBX_MeshData.selected
        if substring nodeName 1 7 != "[Total]" then
        (
            local tempNode = getnodebyname nodeName
            if tempNode != undefined then
            (
                select tempNode
            )
        )
        else
        (
            max select all
        )
    )

    fn CollapseAllNodesToPolygon = 
    (
        for geo in geometry do
        (
            collapseStack geo
            convertToPoly geo
        )
    )

    on lstCheckFBX_MeshData selected index do
    (
        lstCheckFBX_MeshFaces.selection = index
        lstCheckFBX_MeshVerts.selection = index
        SelectListCheckSelectedFBXMesh()
    )

    on lstCheckFBX_MeshFaces selected index do
    (
        lstCheckFBX_MeshData.selection = index
        lstCheckFBX_MeshVerts.selection = index
        SelectListCheckSelectedFBXMesh()
    )

    on lstCheckFBX_MeshVerts selected index do
    (
        lstCheckFBX_MeshData.selection = index
        lstCheckFBX_MeshFaces.selection = index
        SelectListCheckSelectedFBXMesh()
    )

    on edtFBXTool_FBXPath entered text do
    (
        FBXToolFileRoot = edtFBXTool_FBXPath.text
    )

    on edtOptMannalFBX_OpenFBXFile entered text do
    (
        optFBXManualFBXFile = edtOptMannalFBX_OpenFBXFile.text
    )

    on btnOptMannualFBX_ImportFBX pressed do
    (
        Fn_SetDefaultFBXImportSetting "model"
        resetMaxFile #noprompt
        importFile (FBXToolFileRoot + "\\" + optFBXManualFBXFile) #noPrompt using:FbxImporter
        print("Manual Import FBX " + FBXToolFileRoot + "\\" + optFBXManualFBXFile)

        CheckFBXCleanListData()
        if doCheckWhenImport then
        (
            local logString = ""

            local fbxFileName = "Current Scene"
            CheckFBXGetMeshData fbxFileName checkFBXOnlyShowCheckFailed true &logString
            logString = trimRight logString

            local SingleExportRoot = "C:\\ArtToolsLog\\Max2016"
            Fn_ExportTxtFile SingleExportRoot "CheckFBX.txt" logString
        )
    )

    on chkOptMannualFBX_ImportFBX changed theState do
    (
        doCheckWhenImport = theState
    )

    on btnOptMannualFBX_AddLODModifier pressed do
    (
        print("btnOptMannualFBX_AddLODModifier")
        local curSelection = $
        print(curSelection)
        
        for geo in curSelection where (isKindOf geo GeometryClass) do
        (
            print("select " + geo.name)
            local bHavePolygonCruncher = false
            for modifier in geo.modifiers do
            (
                if classof(modifier) == Polygon_Cruncher then
                (
                    bHavePolygonCruncher = true
                )
            )
            
            if not bHavePolygonCruncher then
            (
                print("addmodifier " + geo.name)
                addmodifier geo (Polygon_Cruncher())
                geo.modifiers[#Polygon_Cruncher].Calculate = on
                geo.modifiers[#Polygon_Cruncher].KeepUV = on
                geo.modifiers[#Polygon_Cruncher].UVMode = 1
            )
        )

        max modify mode
    )

    on btnOptMannualFBX_ExportFBXOverride pressed do
    (
        CollapseAllNodesToPolygon()
        Fn_SetDefaultFBXExportSetting "model"

        local exportFilePath = FBXToolFileRoot + "\\" + optFBXManualFBXFile
        exportFile exportFilePath #noprompt selectedOnly:false using:FBXEXP
        print("Manual Export FBX " + exportFilePath)
    )

    on btnOptMannualFBX_ExportFBXSubFolder pressed do
    (
        CollapseAllNodesToPolygon()
        Fn_SetDefaultFBXExportSetting "model"

        local exportFileDir = FBXToolFileRoot + "\\" + optFBXManualFBXSubFolder
        if makeDir exportFileDir all:true then
        (
            local exportFilePath = exportFileDir + "\\" + optFBXManualFBXFile
            exportFile exportFilePath #noprompt selectedOnly:false using:FBXEXP
            print("Manual Export FBX " + exportFilePath)
        )
    )

    on edtOptMannalFBX_ExportFBXSubFolder entered text do
    (
        optFBXManualFBXSubFolder = edtOptMannalFBX_ExportFBXSubFolder.text
    )
)

if ARolloutFloater != undefined then
(
    closeRolloutFloater ARolloutFloater
)
ARolloutFloater = newRolloutFloater "FBX面数工具" 400 500
addrollout MeshLODToolRollout ARolloutFloater