
rollout AddRootOriginRollout "添加Root和*Origin"
(
    button btnAddRoot "添加"

    on btnAddRoot pressed do
    (
        if $ != undefined then
        (
            if getnodebyname "Root" == undefined then
                Dummy pos:[0,0,0] name:"Root" boxSize:[20,20,20]
            if getnodebyname "*origin" == undefined then
                Dummy pos:[0,0,0] name:"*origin" boxSize:[10,10,10]
            
            $.parent = $Root
            messageBox("添加Root和*Origin点成功!    ")
        )
        else
        (
            messageBox("请选中骨架的根节点!    ")
        )
    )
)
AddRootOriginRolloutFloater = newRolloutFloater "位置旋转约束工具" 200 100
addrollout AddRootOriginRollout AddRootOriginRolloutFloater