-- 数据转移 START
if hasINISetting A2UConfigPath "" "dir" then
(
    local exportDirOld = getINISetting A2UConfigPath "" "dir"
    setINISetting A2UConfigPath "FBXExport" "Gate_Dir" exportDirOld
    delIniSetting A2UConfigPath "" "dir"
)

if hasINISetting A2UConfigPath "FBXExport" "dir_gate" then
(
	local exportDirOld = getINISetting A2UConfigPath "FBXExport" "dir_gate"
    setINISetting A2UConfigPath "FBXExport" "Gate_Dir" exportDirOld
    delIniSetting A2UConfigPath "FBXExport" "dir_gate"
)

if hasINISetting A2UConfigPath "FBXExport" "dir_gujian4" then
(
	local exportDirOld = getINISetting A2UConfigPath "FBXExport" "dir_gujian4"
    setINISetting A2UConfigPath "FBXExport" "Gujian4_Dir" exportDirOld
    delIniSetting A2UConfigPath "FBXExport" "dir_gujian4"
)

if (hasINISetting A2UConfigPath "WorkSpace" "Name") == false then
(
    setINISetting A2UConfigPath "WorkSpace" "Name" "Gate"
)
-- 数据转移 END