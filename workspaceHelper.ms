Gate_Rollout = #()						-- 记录Gate项目需要的Rollout
Gate_Rollout_RolledUp = #()				-- 记录Gate项目需要的Rollout
Gujian4_Rollout = #()					-- 记录GuJian4项目需要的Rollout
Gujian4_Rollout_RolledUp = #()			-- 记录GuJian4项目需要的Rollout

fn A2UFn_GetWorkSpace = 
(
	local workSpaceName = getINISetting A2UConfigPath "WorkSpace" "Name"
	return workSpaceName
)

fn A2UFn_IsWorkSpace_Gate = 
(
	return (A2UFn_GetWorkSpace() == "Gate")
)

fn A2UFn_IsWorkSpace_Gujian4 = 
(
	return (A2UFn_GetWorkSpace() == "Gujian4")
)

fn A2UFn_SetWorkSpace_Gate = 
(
	setINISetting A2UConfigPath "WorkSpace" "Name" "Gate"
	return true
)

fn A2UFn_SetWorkSpace_Gujian4 = 
(
	setINISetting A2UConfigPath "WorkSpace" "Name" "Gujian4"
	return true
)

fn A2UFn_GetFBXExportPath_Gate = 
(
    local exportPath = getINISetting A2UConfigPath "FBXExport" "Gate_Dir"
    return exportPath
)

fn A2UFn_GetFBXExportPath_Gujian4 = 
(
    local exportPath = getINISetting A2UConfigPath "FBXExport" "Gujian4_Dir"
    return exportPath
)

fn A2UFn_SetFBXExportPath_Gate text = 
(
    setINISetting A2UConfigPath "FBXExport" "Gate_Dir" text
)

fn A2UFn_SetFBXExportPath_Gujian4 text = 
(
    setINISetting A2UConfigPath "FBXExport" "Gujian4_Dir" text
)

fn A2UFn_AddWorkspaceRollout _workspace _rollout _rolledup =
(
    if _workspace == "Gate" then
    (
		append Gate_Rollout _rollout
		append Gate_Rollout_RolledUp _rolledup
    )
    else if _workspace == "Gujian4" then
    (
		append Gujian4_Rollout _rollout
		append Gujian4_Rollout_RolledUp _rolledup
    )
)

fn A2UFn_ResetRollouts = 
(
	for i = 1 to Gate_Rollout.count do
	(
		if Gate_Rollout[i] != undefined then removeRollout Gate_Rollout[i]		
	)
	for i = 1 to Gujian4_Rollout.count do
	(
		if Gujian4_Rollout[i] != undefined then removeRollout Gujian4_Rollout[i]		
	)
	
	if A2UFn_IsWorkSpace_Gate() then
	(
		for i = 1 to Gate_Rollout.count do
		(
			addRollout Gate_Rollout[i] rolledUp:Gate_Rollout_RolledUp[i]
		)
	)
	else if A2UFn_IsWorkSpace_Gujian4() then
	(
		for i = 1 to Gujian4_Rollout.count do
		(
			addRollout Gujian4_Rollout[i] rolledUp:Gujian4_Rollout_RolledUp[i]
		)
	)
)