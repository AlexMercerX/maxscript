if SetConstraintRolloutFloater != undefined then
(
    closeRolloutFloater SetConstraintRolloutFloater
)

rollout SetConstraintRollout "位置旋转约束工具"
(
    group ""
    (
    button btnPickConstraint "设置被约束物体" align:#left width:100 across:2 offset:[0,-6]
    label lblConstraintObjects "当前: 无" align:#left offset:[-70,-3]
    button btnPickTargetObjects "设置约束目标" align:#left width:100 across:2
    label lblTargetObjects "当前: 无" align:#left offset:[-70,3]
    )

    group ""
    (
    label lblPositionConstraintTypes "位置约束 Position Constraint" width:150 align:#left offset:[5,-6]
    checkbox chkSetPositionConstraint "启用" offset:[5,0] across:2
    checkbox chkSetPositionConstraintRelative "保持初始相对位置" offset:[-100,2] enabled:false
    )

    group ""
    (
    label lblOrientationConstraintTypes "旋转约束 Orientation Constraint" width:200 align:#left offset:[5,-6]
    checkbox chkSetOrientationConstraint "启用" offset:[5,0] across:3
    checkbox chkSetOrientationConstraintRelative "保持初始相对角度" offset:[-40,2] enabled:false
    checkbox chkSetOrientationConstraintLocalWorld "用 Local 角度约束" offset:[-20,2] enabled:false
    )

    button btnExecuteConstraint "设置约束关系" width:300 offset:[0,6]

    local ConstraintObjects = #()
    local TargetObjects = #()

    on chkSetPositionConstraint changed state do
    (
        chkSetPositionConstraintRelative.enabled = state
    )

    on chkSetOrientationConstraint changed state do
    (
        chkSetOrientationConstraintRelative.enabled = state
        chkSetOrientationConstraintLocalWorld.enabled = state
    )

    on btnPickConstraint pressed do
    (
        ConstraintObjects = GetCurrentSelection()

        if ConstraintObjects.count == 0 then
        (
            lblConstraintObjects.text = "当前: 无"
        )
        else
        (
            lblConstraintObjects.text = ("共" + ConstraintObjects.count as string + "个（")
            for i = 1 to ConstraintObjects.count do
            (
                lblConstraintObjects.text = lblConstraintObjects.text + ConstraintObjects[i].name
                if i != ConstraintObjects.count then
                    lblConstraintObjects.text = lblConstraintObjects.text + " / "
            )
            lblConstraintObjects.text = lblConstraintObjects.text + "）"
        )
    )

    on btnPickTargetObjects pressed do
    (
        TargetObjects = GetCurrentSelection()

        if TargetObjects.count == 0 then
        (
            TargetObjects.text = "当前: 无"
        )
        else
        (
            lblTargetObjects.text = ("共" + TargetObjects.count as string + "个（")
            for i = 1 to TargetObjects.count do
            (
                lblTargetObjects.text = lblTargetObjects.text + TargetObjects[i].name
                if i != TargetObjects.count then
                    lblTargetObjects.text = lblTargetObjects.text + " / "
            )
            lblTargetObjects.text = lblTargetObjects.text + "）"
        )
    )

    on btnExecuteConstraint pressed do
    (
        if ConstraintObjects.count == 0 then
        (
            messagebox("没有设置被约束物体       ")
            return false
        )
        
        if TargetObjects.count == 0 then
        (
            messagebox("没有设置约束目标       ")
            return false
        )

        for obj in ConstraintObjects do
        (
            local posCst = Position_Constraint relative:(chkSetPositionConstraintRelative.checked)
            local posCstInterface = posCst.constraints

            local oriCstLocalOrWorld = 0
            if chkSetOrientationConstraintLocalWorld.checked then
                oriCstLocalOrWorld = 1
            local oriCst = Orientation_Constraint relative:(chkSetOrientationConstraintRelative.checked) local_world:oriCstLocalOrWorld
            local oriCstInterface = oriCst.constraints

            for obj in TargetObjects do
            (
                if chkSetPositionConstraint.checked then
                    posCstInterface.appendTarget obj 50.0
                if chkSetOrientationConstraint.checked then
                    oriCstInterface.appendTarget obj 50.0
            )
            
            if chkSetPositionConstraint.checked then
                obj.pos.controller = posCst
            if chkSetOrientationConstraint.checked then
                obj.rotation.controller = oriCst
        )
        
        ConstraintObjects = #()
        TargetObjects = #()
        lblConstraintObjects.text = "当前: 无"
        lblTargetObjects.text = "当前: 无"
    )
)

SetConstraintRolloutFloater = newRolloutFloater "位置旋转约束工具" 400 260
addrollout SetConstraintRollout SetConstraintRolloutFloater