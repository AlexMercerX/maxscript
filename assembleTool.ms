rollout assembleAnimDialog "动画组装工具" width:400 height:820
(	
	global playblastWidth = 960
	global playblastHeight = 540

	-- import character fbx with animation
	fn importCharFbxWithAnim charFbxWithAnimPath =
	(	
		FBXImporterSetParam "Mode" #create
		FBXImporterSetParam "Animation" True
		if charFbxWithAnimPath.count > 0 then (importFile charFbxWithAnimPath #noPrompt using:FbxImporter)
	)


	-- export animation
	fn exportSelectedNodeAnimData startFrame endFrame animExportPath =
	(	
		animExportDir = getFilenamePath animExportPath
		if not (doesFileExist animExportDir) then makeDir animExportDir
		LoadSaveAnimation.saveAnimation animExportPath $ #("") #("") animatedTracks:True includeConstraints:True keyableTracks:True SaveSegment:True segInterval:(interval startFrame  endFrame) segKeyPerFrame:True
	)

	-- import animation
	fn importAnimDataToSelectedNode startFrame endFrame animImportPath =
	(
		LoadSaveAnimation.loadAnimation animImportPath $ relative:False insert:False
		animationRange = interval startFrame endFrame -- set frame range
	)

	-- import camera fbx 
	fn importCameraFbx cameraFbxPath = 
	(
		FBXImporterSetParam "Mode" #create
		FBXImporterSetParam "Animation" True
		if cameraFbxPath.count > 0 then (importFile cameraFbxPath #noPrompt using:FbxImporter)
	)


	-- transfer camera attributes
	fn tranferCameraAttr = 
	(
		max modify mode
		set animate on
		if cameras.count > 0 then
		(
			for cam in cameras do
			(
				if (hasProperty cam "fieldOfView" == false) do continue
				fieldOfViewController = cam.fieldOfView.controller
				keyCount = numkeys fieldOfViewController
				startTime = getkeytime cam.position.controller 1
				
				for i = 1 to keyCount do 
				(
					keyFrame = getkeytime fieldOfViewController i
					at time keyFrame
					(
						cam.fov = at time keyFrame cam.fieldOfView
					)
				)
			)
			if viewport.numViews > 1 do (
				max tool maximize
				viewport.setcamera cameras[1]
			)
		)
		set animate off
	)

	-- import prop fbxes with animation
	fn importPropFbxesWithAnim propFbxPaths =
	(
		for path in propFbxPaths do
		(
			FBXImporterSetParam "Mode" #create
			FBXImporterSetParam "Animation" True
			importFile path #noPrompt using:FbxImporter
		)	
	)

	-- import scene fbxes without animation
	fn importScFbxesWithoutAnim scFbxPath =
	(
		FBXImporterSetParam "Mode" #merge
		FBXImporterSetParam "Animation" False
		if scFbxPath.count > 0 then (importFile scFbxPath #noPrompt using:FbxImporter)
	)


	-- save file
	fn saveFile savePath = 
	(
		saveMaxFile (savePath + ".max") clearNeedSaveFlag:True useNewFile:True
	)	

	-- export abc
	fn exportAbcFile exportPath =
	(
		AlembicExport.ArchiveType = #ogawa --force the new file format
		AlembicExport.CoordinateSystem = #maya --force Y up coordinates
		exportFile (exportPath + ".abc") #noPrompt selectedOnly:False using:AlembicExport
	)

	-- export fbx
	fn exportFbxFile exportPath =
	(
		select $...*
		$.displayByLayer=True
		clearSelection()
		exportFile (exportPath + ".fbx") #noPrompt selectedOnly:False using:FbxExporter
	)

	fn getMultiOpenFileName caption types =
	(
		theDialog = dotNetObject "System.Windows.Forms.OpenFileDialog" --create a OpenFileDialog
		theDialog.title = caption --set the title
		theDialog.Multiselect = true --allow multiple files to be selected
		theDialog.Filter = types --specify the filter like "HTML Files (*.html)|*.html|All Files (*.*)|*.*"
		-- theDialog.FilterIndex = 2 --set the filter drop-down list to All Files
		result = theDialog.showDialog() --display the dialog, get result into variable
		result.ToString() --when closed, convert the result to string
		result.Equals result.OK --returns TRUE if Open was pressed, FALSE otherwise
		result.Equals result.Cancel --returns TRUE if Cancel was pressed, FALSE otherwise
		theFilenames = theDialog.fileNames --the selected filenames will be returned as an array
		return theFilenames
	)

	edittext startFrameEditText "起始帧:" fieldWidth:50 text:"0" align:#center across:3
	edittext endFrameEditText "结束帧:" fieldWidth:50 text:"0"
	edittext fpsEditText "帧速率:" fieldWidth:50 text:"30"
	group "1.蓝图动画组装"
	(
		edittext charFbxEditText "蓝图动画 fbx 路径:" 
		button addCharAnimFbxButton "添加蓝图动画路径" toolTip:"从UE Level Seuquence导出的蓝图动画fbx" align:#center --across:2 offset:[0,2]
		
		multiListBox meshList "SK 模型组件 fbx 路径:" height:7 items:#() readOnly:false
		button addMeshButton "添加 SK 模型路径" toolTip:"选择UE导出的蓝图相关 SK 模型组件，将路径添加到上面的列表中" align:#center across:2
		button deleteMeshButton "删除 SK 模型路径" toolTip:"删除上面的列表中选择的路径" align:#center
		label seprator1 "————————————————————————————————————"
		button assembleCharAnimButton "组装蓝图动画" toolTip:"新建场景，将蓝图动画传给SK模型组件" align:#center 
	)
	
	on addCharAnimFbxButton pressed do
	(	
		filePath = getOpenFileName \
		caption:"选择蓝图动画 fbx" \
		types:"Fbx(*.fbx)|*.fbx"
		-- print filePath
		if filePath != undefined then charFbxEditText.text = filePath
	)

	on addMeshButton pressed do
	(	
		caption = "选择蓝图动画 fbx"
		types = "Fbx(*.fbx)|*.fbx"
		filePaths = getMultiOpenFileName caption types

		if filePaths != #() then
		(
			tempArray = meshList.items
			for path in filePaths do
			(	
				if (findItem tempArray path) != 0 do continue
				meshList.items = append meshList.items path
			)
		)
	)

	on deleteMeshButton pressed do
	(	
		meshList.items = for i = 1 to meshList.items.count where not meshList.selection[i] collect meshList.items[i]
		meshList.selection = #{}
	)

	fn initScene = 
	(
		resetMaxFile #noPrompt
		frameRate = fpsEditText.text as integer
		preferences.spinnerPrecision = 6
	)

	-- import multi splited character fbxes
	fn importMultiSplitedCharFbxes splitedCharFbxes =
	(
		initScene()
		if splitedCharFbxes.count > 0 then
		(
			for path in splitedCharFbxes do
			(
				FBXImporterSetParam "Mode" #merge
				FBXImporterSetParam "Animation" False
				importFile path #noPrompt using:FbxImporter
			)
		)
	)

	fn assembleCharAnim =
	(
		initScene()

		charFbxWithAnimPath = charFbxEditText.text
		importCharFbxWithAnim charFbxWithAnimPath

		startFrame = startFrameEditText.text as integer
		endFrame = endFrameEditText.text as integer

		skeletonAnimExportPath = "D:/AurogonData/animExportData/skeletonAnim.xaf"
		rootAnimExportPath = "D:/AurogonData/animExportData/rootAnim.xaf"

		clearSelection()
		skeletalRootNodeName = "SkeletalMeshComponent0"	
		execute ("select $" + skeletalRootNodeName + "...Root...*") -- select Root under skeletalRootNode with all children
		exportSelectedNodeAnimData startFrame endFrame skeletonAnimExportPath

		select $[1].parent.parent
		rootNodeName = $.name
		at time 0 rootNodePosition = $.position
		exportSelectedNodeAnimData startFrame endFrame rootAnimExportPath

		splitedCharFbxes = meshList.items
		importMultiSplitedCharFbxes splitedCharFbxes

		dmmy = Dummy name:rootNodeName
		$Root.parent = dmmy

		clearSelection()
		select dmmy
		importAnimDataToSelectedNode startFrame endFrame rootAnimExportPath

		clearSelection()	
		execute ("select $" + "Root...*") -- select Root with all children
		importAnimDataToSelectedNode startFrame endFrame skeletonAnimExportPath
	)

	on assembleCharAnimButton pressed do assembleCharAnim()

	group "2.SK/SM 动画、静态场景、相机组装"
	(
		edittext camFbxEditText "相机动画 fbx 路径:" 
		button addCamAnimFbxButton "添加相机动画路径" toolTip:"从UE Level Seuquence导出的相机动画 fbx（不包含Target）" align:#center --across:2 offset:[0,2]
		edittext scFbxEditText "静态场景 fbx 路径:" 
		button addScFbxButton "添加静态场景路径" toolTip:"从UE导出的静态场景 fbx" align:#center --across:2 offset:[0,2]
		
		multiListBox propAnimList "SK/SM 动画 fbx 路径:" height:7 items:#() readOnly:false
		button addPropAnimFbxButton "添加 SK/SM 动画路径" toolTip:"选择从UE Level Seuquence导出的 SK/SM 动画（一般是道具），将路径添加到上面的列表中" align:#center across:2
		button deletePropAnimFbxButton "删除 SK/SM 动画路径" toolTip:"删除上面的列表中选择的路径" align:#center
		label seprator2 "————————————————————————————————————"
		button assembleCamScAnimButton "组装 SK/SM 动画静态场景和相机" toolTip:"将静态场景、SK/SM动画、相机动画导入当前文件，并将相机的自定义属性上的动画传到相应位置" align:#center 
	)

	on addCamAnimFbxButton pressed do
	(	
		filePath = getOpenFileName \
		caption:"选择相机动画 fbx" \
		types:"Fbx(*.fbx)|*.fbx"
		if filePath != undefined then camFbxEditText.text = filePath
	)

	on addScFbxButton pressed do
	(	
		filePath = getOpenFileName \
		caption:"选择静态场景 fbx" \
		types:"Fbx(*.fbx)|*.fbx"
		if filePath != undefined then scFbxEditText.text = filePath
	)

	on addPropAnimFbxButton pressed do
	(	
		caption = "选择 SK/SM 动画 fbx"
		types = "Fbx(*.fbx)|*.fbx"
		filePaths = getMultiOpenFileName caption types

		if filePaths != #() then
		(
			tempArray = propAnimList.items
			for path in filePaths do
			(	
				if (findItem tempArray path) != 0 do continue
				propAnimList.items = append propAnimList.items path
			)
		)
	)

	on deletePropAnimFbxButton pressed do
	(	
		propAnimList.items = for i = 1 to propAnimList.items.count where not propAnimList.selection[i] collect propAnimList.items[i]
		propAnimList.selection = #{}
	)

	fn assembleCamScAnim =
	(
		propFbxPaths = propAnimList.items
		importPropFbxesWithAnim propFbxPaths

		scFbxPath = scFbxEditText.text
		importScFbxesWithoutAnim scFbxPath
		
		cameraFbxPath = camFbxEditText.text
		importCameraFbx cameraFbxPath
		tranferCameraAttr()
	)
	on assembleCamScAnimButton pressed do assembleCamScAnim()
	
	group "3.文件导出"
	(
		radiobuttons fileTypeRadiobuttons labels:#("abc", "fbx", "abc 和 fbx") default:3

		edittext exportEditText "导出路径:" 
		button exportPathButton "添加导出路径" toolTip:"储存 max 文件，导出 abc / fbx 文件的位置" align:#center
		
		label seprator3 "————————————————————————————————————"
		button saveAndExportButton "储存 max 文件，导出 abc / fbx" toolTip:"储存当前max文件并导出选择的文件类型" align:#center 

	)

	group "4.拍屏"
	(
		button playblastButton "拍屏" toolTip:"拍屏，注意拍屏之前将画面拉大" align:#center
	)

	on exportPathButton pressed do
	(
		filePath = getSaveFileName \
		caption:"选择储存位置" \
		types:"All Files (*.*)|*.*"
		startFrame = startFrameEditText.text
		endFrame = endFrameEditText.text
		
		if filePath != undefined then 
		(
			filePath += "_" + startFrame + "_" + endFrame
			exportEditText.text = filePath
		)
	)

	fn saveAndExport =
	(
		exportPath = exportEditText.text
		saveFile exportPath

		radioButtonState = fileTypeRadiobuttons.state
		if radioButtonState == 1 then exportAbcFile exportPath
		else if radioButtonState == 2 then exportFbxFile exportPath
		else 
		(
			exportAbcFile exportPath
			exportFbxFile exportPath
		)
	)

	on saveAndExportButton pressed do saveAndExport()
	
	fn doPlayblast width height =
	(
		-- show only geometry
		max unhide all
		max hide camera toggle
		max hide helper toggle
		max hide light toggle
		max hide bone object toggle
		max hide shape toggle
		max hide system toggle
		max hide wsm toggle
		
		renderWidth = playblastWidth
		renderHeight = playblastHeight

		exportPath = exportEditText.text
		exportFullPathFin = exportPath + ".mp4"
		exportDir = getFilenamePath exportPath
		startFrame = startFrameEditText.text as integer
		endFrame = endFrameEditText.text as integer
		frameRate = fpsEditText.text as integer
		-- animBmp = bitmap view_size.x view_size.y filename:exportFullPath
		-- -- for t = animationrange.start to animationrange.end do
		-- for t = startFrame to endFrame do
		-- (
		-- 	sliderTime = t
		-- 	dib = gw.getViewportDib()
		-- 	copy dib animBmp
		-- 	save animBmp
		-- )
		-- close animBmp
		-- gc()
		
		pathConfig.SetDir #preview exportDir
		createPreview percentSize:100 \
		start:startFrame end:endFrame skip:1 fps:30 \
		dspGeometry:true dspShapes:false dspLights:false \
		dspCameras:false dspHelpers:false dspParticles:false dspBones:false \
		dspGrid:false dspSafeFrame:false dspFrameNums:true dspBkg:false \
		rndLevel:#smooth
		-- pathConfig.SetDir #preview exportDir
		aviFilePath = exportDir + "_scene.avi"

		ffmpegPath = "//192.168.21.16/ArtTools/thirdparty/ffmpeg/bin/ffmpeg.exe"
		theCommand = ffmpegPath + " -i " + aviFilePath + " -x264opts b_pyramid=0 -c:v libx264 -vf format=yuv420p -y " + exportFullPathFin
		DOSCommand (theCommand)
		sleep 1
		deletefile aviFilePath
	)

	on playblastButton pressed do doPlayblast playblastWidth playblastHeight

	button batchingButton "一键组装，导出，拍屏" toolTip:"依次执行3步操作：\n1.组装蓝图动画\n2.组装相机、SK/SM 动画和静态场景\n3.储存 max 文件并导出 abc / fbx\n4.拍屏" align:#center 

	on batchingButton pressed do
	(
		assembleCharAnim()
		assembleCamScAnim()
		saveAndExport()
		doPlayblast playblastWidth playblastHeight
	)
)
