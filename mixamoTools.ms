rollout MixamoRollout "添加Root和*Origin"
(
    label lblFbxDir "Mixamo文件目录" across:2 align:#left
    editText edtFbxDir "" align:#left
    button btnBatchAddRoot "批量添加Root点"
    
    button btnAddRoot "添加Root点"

    -- files = getFiles "G:\\TA\\Mixamo\\*.fbx"

    on btnAddRoot pressed do
    (
        if $ != undefined then
        (
            if getnodebyname "Root" == undefined then
                Dummy pos:[0,0,0] name:"Root" boxSize:[20,20,20]
            if getnodebyname "*origin" == undefined then
                Dummy pos:[0,0,0] name:"*origin" boxSize:[10,10,10]
            
            $.parent = $Root
            messageBox("添加Root和*Origin点成功!    ")
        )
        else
        (
            messageBox("请选中骨架的根节点!    ")
        )
    )
)

MixamoRolloutFloater = newRolloutFloater "Mixamo工具" 200 400
addrollout MixamoRollout MixamoRolloutFloater